# %% Load the packages
import pandas as pd
import numpy as np
import plotly.express as plx
import plotly.graph_objects as go

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bin_data_tutto"
X_raw = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "target_data_tutto"
df_target = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Make one hot data
X_raw_dummy = X_raw.copy()
X_raw_dummy[X_raw > 0] = 1

# %% Set folder where to store images in
data_folder = "plots/TargetsDistribution/"

# %% Set dataset
mask_dataset = "NatWald"
mask = df_target["dataset"] == mask_dataset
# %% Distribution of habitat type

title_string = f"Distribution of the Habitat Types"

fig = go.Figure()

fig = plx.histogram(df_target["habitat_type"],
    title = title_string,
    color = df_target["dataset"],
    opacity = 1,
)

#filename = f"Distribution_of_habitat_type"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Distribution of month 

title_string = f"Distribution of the Month ({mask_dataset})"

fig = go.Figure()

fig = plx.histogram(df_target.loc[mask, "month"],
    title = title_string,
    color = df_target.loc[mask, "dataset"],
    opacity = 1,
)
fig.update_layout(
    xaxis = dict(
        tickmode = 'array',
        tickvals = [date for date in sorted(set(df_target["month"]))],
        ticktext = [date.month_name() for date in sorted(set(df_target["month"]))]),
    bargap=0.15)

Load_Store_Data().saving(fig, data_folder, title_string, "html")

# %%
set(df_target["dataset"])
# %% Distribution of habitat type + month 

title_string = f"Distribution of the Habitat Types and Month"

fig = go.Figure()

fig = plx.histogram(df_target["habitat_type"],
    title = title_string,
    color = df_target["dataset"],
    facet_col=df_target["month"].dt.month_name(),
    category_orders={"facet_col": [date.month_name() for date in sorted(set(df_target["month"]))]},
    labels = {"facet_col" : "month"},
    opacity = 1,
)

filename = f"Distribution_of_habitat_type_month"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Distribution of trap types

title_string = f"Distribution of the Trap Types"

fig = go.Figure()
fig = plx.histogram(df_target["trap_type"],
    title = title_string,
    color = df_target["dataset"],
    opacity = 1,
)

filename = f"Distribution_of_trap_type"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Distribution of latitude and longitude

title_string = f"Distribution of the Latitude and Longitude"

fig = plx.histogram(df_target["longitude"].round(0).astype("int"),
    title = title_string,
    color = df_target["dataset"],
    facet_row=df_target["latitude"].round(0).astype("int"),
    category_orders={"facet_row": [lon for lon in sorted(set(df_target["latitude"].round(0).astype("int")), reverse=True)]},
    labels = {"facet_row" : "°"},
    width=1000,
    opacity = 1,
)
fig.update_layout(bargap=0.15)

filename = f"Distribution_of_latitude_longitude"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Grouped table to show target distribution
df_temp = df_target.copy()
df_temp["month"] = df_temp["month"].dt.month
df_temp["latitude"] = df_target["latitude"].round(0).astype("int")
df_temp.rename({"observation_id" : "observations"}, axis=1, inplace=True)
df_temp.groupby(["habitat_type", "month"]).count()
# %%
