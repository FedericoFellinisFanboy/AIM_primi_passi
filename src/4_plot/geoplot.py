# %% Load the packages

import pandas as pd
import plotly.express as plx
import plotly.graph_objects as go

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %%

data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
df_all = Load_Store_Data().loading(data_folder, filename, "parquet")
# df_all["month"] = df_all["month"].dt.month_name()

filename = "target_data_tutto"
df_target = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Set folder where to store images in
data_folder = "/plots/maps/"

# %% Plot by habitat type

# fig = go.Figure(data =
#     go.Scattermapbox(lat=df_target["lat"], lon=df_target["lon"],
#                         mode="markers",
#                         marker=go.scattermapbox.Marker(size=8, symbol="hospital")
#                         #marker_symbol=df_target["month"]
#                         #text=[df_target["habitat_type"], df_target["month"]]
                
# ))

fig = plx.scatter_mapbox(df_target, lat="latitude", lon="longitude", 
                            hover_data={"month": df_target["month"].dt.month_name(), "dataset" : True},
                            color = df_target["habitat_type"],
                            opacity=0.5)

fig.update_traces(marker=dict(size=8))
fig.update_layout(mapbox_style="open-street-map", 
                    mapbox_zoom=4, 
                    mapbox_center=go.layout.mapbox.Center(
                                    lat=50,
                                    lon=11
        ))
fig.update_layout(legend=dict(
    yanchor="top",
    y=0.95
))
fig.update_layout(margin={"r":100,"t":0,"l":0,"b":0})

filename = "geoplot_by_habitat_type"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %%

# %% Plot by month
fig = plx.scatter_mapbox(df_target, lat="latitude", lon="longitude", 
                            hover_data={"habitat_type" : True, "dataset" : True},
                            color = df_target["month"].dt.month_name(),
                            category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
                            labels = {"color" : "month"},
                            opacity=0.5)

fig.update_traces(marker=dict(size=8, allowoverlap = False ))
fig.update_layout(mapbox_style="open-street-map", 
                    mapbox_zoom=4, 
                    mapbox_center=go.layout.mapbox.Center(
                                    lat=50,
                                    lon=11
        ))
fig.update_layout(legend=dict(
    yanchor="top",
    y=0.95
))
fig.update_layout(margin={"r":100,"t":0,"l":0,"b":0})

filename = "geoplot_by_month"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Plot by dataset
fig = plx.scatter_mapbox(df_target, lat="latitude", lon="longitude", 
                            hover_data={"habitat_type" : True, 
                                        "month": df_target["month"].dt.month_name()},
                            color = df_target["dataset"],
                            opacity=0.5)

fig.update_traces(marker=dict(size=8, allowoverlap = False ))
fig.update_layout(mapbox_style="open-street-map", 
                    mapbox_zoom=4, 
                    mapbox_center=go.layout.mapbox.Center(
                                    lat=50,
                                    lon=11
        ))
fig.update_layout(legend=dict(
    yanchor="top",
    y=0.95
))
fig.update_layout(margin={"r":100,"t":0,"l":0,"b":0})

filename = "geoplot_by_dataset"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Unify the geometry data using Geopandas

import geopandas
import geopy.distance
import numpy as np

df_all_geo = geopandas.GeoDataFrame(df_all, geometry = geopandas.points_from_xy(df_all["longitude"], 
                                                                            df_all["latitude"]))

df_target_geo = geopandas.GeoDataFrame(df_target, geometry = geopandas.points_from_xy(df_target["longitude"], 
                                                                            df_target["latitude"]))

world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))

country_mask = world.name=="Germany"

base = world[country_mask].plot(color='white', edgecolor='black')
df_target_geo.plot(ax=base, marker='o', color='red', markersize=5)

# %%
mask=df_all_geo["AEJ9383"] > 0

base = world[country_mask].plot(color='white', edgecolor='black')
df_target_geo[mask].plot(ax=base, marker='o', color='red', markersize=5)

# %%
center = df_target_geo.dissolve().centroid

base = world[country_mask].plot(color='white', edgecolor='black')
center.plot(ax=base, marker="x", color = "green", markersize=50)

# %%
geodata = [(x,y) for x,y in zip(df_target["latitude"], df_target["longitude"])]
dist = np.array([geopy.distance.geodesic((x,y), (float(center.x), float(center.y))).meters / 1000 for (x,y) in geodata])

base = world[country_mask].plot(color='white', edgecolor='black')

mask = (dist == np.max(dist))

df_target_geo[mask].plot(ax=base, marker="d", color = "purple", markersize=50)

# %%

# %%
fig = plx.scatter_mapbox(df_target[mask], lat="latitude", lon="longitude", 
                            hover_data={"dataset" : True, 
                                        "habitat_type" : True, 
                                        "month" : True,
                                        "latitude" : True,
                                        "longitude" : True},
                            color = df_target.loc[mask, "month"],
                            category_orders={"month": [date.month_name() for date in sorted(set(df_target["month"]))]},
                            opacity=0.5)

fig.update_traces(marker=dict(size=8, allowoverlap = False ))
fig.update_layout(mapbox_style="open-street-map", 
                    mapbox_zoom=4, 
                    mapbox_center=go.layout.mapbox.Center(
                                    lat=50,
                                    lon=11
        ))
fig.update_layout(legend=dict(
    yanchor="top",
    y=0.95
))
fig.update_layout(margin={"r":100,"t":0,"l":0,"b":0})

filename = "geoplot_far_distance"
Load_Store_Data().saving(fig, data_folder, filename, "html")
