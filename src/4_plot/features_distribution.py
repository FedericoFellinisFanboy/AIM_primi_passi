# %% Load the packages
import pandas as pd
import numpy as np
import plotly.express as plx
import plotly.graph_objects as go
from plotly.subplots import make_subplots

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bin_data_tutto"
X_raw =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "target_data_tutto"
df_target =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bold_bin_consenus_family_connection"
df_bin_consensus_family_connection =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bold_bin_consenus_family_tutto"
df_bin_infos =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "insect_trait_tool"
df_insect_traits =  Load_Store_Data().loading(data_folder, filename, "parquet")

store_img_data_folder = "plots/FeaturesDistribution/"

# %% Make one hot data
X_raw_dummy = X_raw.astype("bool")

# %% Make consensus families dataframes
bold_bin_intersection = set(X_raw.columns).intersection(set(df_bin_consensus_family_connection["bold_bin_uri"]))

X_consensus_family = X_raw.loc[:,bold_bin_intersection].rename({oldCol : newCol for oldCol, newCol in 
                            zip(df_bin_consensus_family_connection["bold_bin_uri"], df_bin_consensus_family_connection["consensus_family"])}, axis=1)
X_consensus_family = X_consensus_family.groupby(by=X_consensus_family.columns, axis=1).sum()

X_consensus_family_normalized = X_consensus_family.apply(lambda x : x / np.sum(X_consensus_family, axis = 1).values)

bold_bin_intersection = set(X_raw.columns).intersection(set(df_bin_consensus_family_connection["bold_bin_uri"]))

X_consensus_family_dummy = X_raw_dummy.loc[:,bold_bin_intersection].rename({oldCol : newCol for oldCol, newCol in 
                            zip(df_bin_consensus_family_connection["bold_bin_uri"], df_bin_consensus_family_connection["consensus_family"])}, axis=1)
X_consensus_family_dummy = X_consensus_family_dummy.groupby(by=X_consensus_family_dummy.columns, axis=1).sum()

# %%

# %%
set(df_bin_consensus_family_connection["bold_bin_uri"]).difference(set(X_raw.columns))

# %% Check for features distribution over all
pd.DataFrame(np.sum(X_raw_dummy,axis=0), columns=["Sum"])["Sum"].describe()

# %% Sum of a BOLD BIN as SVG

title_string = f"Sum of different BOLD BINs along Habitat Type and Month"
fig = plx.scatter(np.sum(X_raw_dummy,axis=1),
    color = df_target["dataset"],
    hover_data={"dataset": df_target["dataset"], "month": df_target["month"].dt.month_name()},
    facet_row = df_target["habitat_type"],
    facet_col = df_target["month"].dt.month_name(),
    category_orders={"facet_col": [date.month_name() for date in sorted(set(df_target["month"]))]},
    title = title_string,
    opacity=1
)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "svg")

# %% Sum of a BOLD BIN

title_string = f"Sum of different BOLD BINs along Habitat Type and Month"
fig = plx.scatter(np.sum(X_raw_dummy,axis=1),
    color = df_target["dataset"],
    hover_data={"dataset": df_target["dataset"], "month": df_target["month"].dt.month_name()},
    facet_row = df_target["habitat_type"],
    facet_col = df_target["month"].dt.month_name(),
    category_orders={"facet_col": [date.month_name() for date in sorted(set(df_target["month"]))]},
    title = title_string,
    opacity=1
)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")

# %% Distribution of BOLD BINs along Habitat Type

title_string = f"Distribution of BOLD BINs along Habitat Type"

habitat_types = ["agriculture", "forest", "nature",  "urban"]
habitat_types = ["forest"]
count_habitat_types = len(habitat_types)

fig = make_subplots(rows=count_habitat_types, cols=1)
for habitat_type, color, i in zip(habitat_types, plx.colors.qualitative.Plotly[:count_habitat_types], range(1, count_habitat_types + 1)):
    mask = df_target["habitat_type"] == habitat_type
    subplot = plx.scatter(np.sum(X_raw_dummy[mask],axis=0),
        color_discrete_sequence=[color],
        title = title_string,
        opacity=1
    )
    subplot.data[0].name= habitat_type
    fig.add_trace(subplot.data[0], row = i, col = 1)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")

# %% Distribution of a consensus family along Habitat Type

title_string = f"Distribution of Consensus Family along Habitat Type"

habitat_types = ["agriculture", "forest", "nature",  "urban"]
habitat_types = ["forest"]

count_habitat_types = len(habitat_types)

fig = make_subplots(rows=count_habitat_types, cols=1)

for habitat_type, color, i in zip(habitat_types, plx.colors.qualitative.Plotly[:count_habitat_types], range(1, count_habitat_types + 1)):
    
    mask = df_target["habitat_type"] == habitat_type
    X_consensus_family = (pd.DataFrame(np.sum(X_raw_dummy[mask],axis=0), columns=["counts"]).reset_index().rename({"index" : "bold_bin_uri"}, axis=1)
                            .merge(df_bin_consensus_family_connection, on='bold_bin_uri')[["consensus_family", "counts"]]
                            .groupby("consensus_family").sum().reset_index())
    
    subplot = plx.scatter(x=X_consensus_family["consensus_family"], y=X_consensus_family["counts"],
        color_discrete_sequence=[color],
        title = title_string,
        opacity=1
    )
    subplot.data[0].name= habitat_type
    fig.add_trace(subplot.data[0], row = i, col = 1)
    fig.update_xaxes(tickangle= -45)  

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")

# %% Distribution of a BOLD BIN along combination of dataset and habitat type"

title_string = f"Distribution of BOLD BINs along combination of dataset and habitat type"

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
count_combinations = len(combination_dataset_habitat_type)

sum_threshold = 50

fig = make_subplots(rows=count_habitat_types, cols=1)
for dataset, habitat_type, color, i in zip(*zip(*combination_dataset_habitat_type), plx.colors.qualitative.Plotly[:count_combinations], range(1, count_combinations + 1)):
    mask = ((df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type))
    subplot = plx.scatter(np.sum(X_raw_dummy[mask],axis=0),
        color_discrete_sequence=[color],
        title = title_string,
        opacity=1
    )

    X_plot = np.sum(X_raw_dummy[mask],axis=0)[np.sum(X_raw_dummy[mask],axis=0) > sum_threshold]

    subplot = go.Bar(x=X_plot.index,
                        y=X_plot,
                        name=f"{dataset}_{habitat_type}",
                        )
    #subplot.data[0].name= f"{dataset}_{habitat_type}"
    #fig.add_trace(subplot.data[0], row = 1, col = 1)
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)  


fig.update_layout(title_text=title_string, barmode="stack")
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(combination_dataset_habitat_type)})", "html")

# %% Distribution of top consensus families along Combination of Dataset and Habitat Type

title_string = f"Distribution of Top Consensus Families along Combination of Dataset and Habitat Type"

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
count_combinations = len(combination_dataset_habitat_type)
sum_threshold = 1000

fig = make_subplots(rows=1, cols=1)
for dataset, habitat_type, color, i in zip(*zip(*combination_dataset_habitat_type), plx.colors.qualitative.Plotly[:count_combinations], range(1, count_combinations + 1)):
    
    mask = ((df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type))
    X_consensus_family = (pd.DataFrame(np.sum(X_raw_dummy[mask],axis=0), columns=["counts"]).reset_index().rename({"index" : "bold_bin_uri"}, axis=1)
                            .merge(df_bin_consensus_family_connection, on='bold_bin_uri')[["consensus_family", "counts"]]
                            .groupby("consensus_family").sum().reset_index())
    X_consensus_family = X_consensus_family[X_consensus_family["counts"] > sum_threshold]

    subplot = go.Bar(x=X_consensus_family["consensus_family"], 
                        y=X_consensus_family["counts"], 
                        name=f"{dataset}_{habitat_type}",
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)  
    
fig.update_layout(title_text=title_string, barmode="stack")
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(combination_dataset_habitat_type)})", "html")


# %% Histogram of one consensus families along Combination of Dataset and Habitat Type

consensus_family = "Phoridae"

title_string = f"Histogram of One Consensus Family ({consensus_family}) along Combination of Dataset and Habitat Type"

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest"), ("LandKLIF", "agriculture")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]
count_combinations = len(combination_dataset_habitat_type)

fig = make_subplots(rows=1, cols=1)
for dataset, habitat_type, color, i in zip(*zip(*combination_dataset_habitat_type), plx.colors.qualitative.Plotly[:count_combinations], range(1, count_combinations + 1)):
    
    mask = ((df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type))
    X_consensus_family = (pd.DataFrame(np.sum(X_raw_dummy[mask],axis=0), columns=["counts"]).reset_index().rename({"index" : "bold_bin_uri"}, axis=1)
                            .merge(df_bin_consensus_family_connection, on='bold_bin_uri')[["consensus_family", "counts"]]
                            .groupby("consensus_family").sum().reset_index())
    X_consensus_family = X_consensus_family[X_consensus_family["consensus_family"] == consensus_family]

    subplot = go.Bar(x=X_consensus_family["consensus_family"], 
                        y=X_consensus_family["counts"], 
                        name=f"{dataset}_{habitat_type}",
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)  

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")

# %%

# %% Get insect trait metadata
def get_insect_trait_metadata_along_consensus_families(X, combination_dataset_habitat_type, _sum_threshold, _columns, extra_factor = None):
    
    mask = np.sum([(df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type) for dataset, habitat_type in combination_dataset_habitat_type], axis=0).astype("bool")

    bold_bin_intersection = set(X.columns).intersection(set(df_insect_traits["consensus_family"])) 

    X = pd.DataFrame(np.sum(X.loc[mask, bold_bin_intersection],axis=0), columns=["counts"])
    
    X_metadata = X.copy(deep=True)
    if not extra_factor:
        for col in _columns:
            X_metadata[col] = X.T.apply(lambda x : x * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values).T

    elif extra_factor in df_insect_traits:
        for col in _columns:
            X_metadata[col] = X.T.apply(lambda x : x 
                                                    * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values
                                                    * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, extra_factor].values).T

    elif isinstance(extra_factor, int, float, bool):
        for col in _columns:
            X_metadata[col] = X.T.apply(lambda x : x 
                                                    * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values
                                                    * extra_factor).T
    
    return X_metadata[X_metadata["counts"] > _sum_threshold].sort_index().reset_index().rename({"index" : "consensus_family"}, axis=1)
# %%

#%%
def get_insect_trait_metadata_along_observations(X, combination_dataset_habitat_type, _columns, extra_factor = None):
    
    mask = np.sum([(df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type) for dataset, habitat_type in combination_dataset_habitat_type], axis=0).astype("bool")
    
    bold_bin_intersection = set(X.columns).intersection(set(df_insect_traits["consensus_family"])) 
    
    X = X.loc[mask, bold_bin_intersection]
    # X = X.rename({oldCol : newCol for oldCol, newCol in zip(df_bin_infos["bold_bin_uri"], df_bin_infos["consensus_family"])}, axis=1)
    # X = X.groupby(by=X.columns, axis=1).sum()

    X_metadata = df_target.loc[mask, ["observation_id"]]

    if not extra_factor:
        for col in _columns:
            X_metadata[col] = np.sum(X.apply(lambda x : x * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values), axis = 1)

    elif extra_factor in df_insect_traits:
        for col in _columns:
            X_metadata[col] = np.sum(X.apply(lambda x : (x 
                                                        * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values)
                                                        * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, extra_factor].values), axis = 1)

    elif isinstance(extra_factor, int, float, bool):
        for col in _columns:
            X_metadata[col] = np.sum(X.apply(lambda x : (x 
                                                        * df_insect_traits.loc[df_insect_traits["consensus_family"] == x.name, col].values)
                                                        * extra_factor), axis = 1)

    return X_metadata


# %%

# %%
def get_insect_trait_insecurities():
    diet_types_of_phytophagous_larvae = ["phyllophagous", "saproxylic", "sap-sucking", "stem", "flower", "seed", "gall-inducing", "miners", "roots", "phytophagous not specified"]
    diet_types_of_zoophagous_larvae = ["predator", "micropredator", "parasite", "parasitoid", "necrophorous", "zoophagous not specified"]

    mask = df_insect_traits["phytophagous"] > 0
    A = df_insect_traits.loc[mask]
    small_mask = np.sum(A[diet_types_of_phytophagous_larvae], axis=1).round(1) != 1
    A.loc[small_mask, ["higher_taxon", "consensus_family"] + diet_types_of_phytophagous_larvae]#.to_csv("insect-trait-tool__?", index=False, sep=";")
    
    return A.loc[small_mask, ["higher_taxon", "consensus_family"] + diet_types_of_phytophagous_larvae]

# %%

# %% Histogram of habitats of larvae and adults for Consensus Family along Combination of Dataset and Habitat Type

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

sum_threshold = 500

habitats_larvae = ["larva_terrestrial", "larva_aquatic"]
habitats_adults = ["adult_terrestrial", "adult_aquatic"]

X_habitat_types = get_insect_trait_metadata_along_consensus_families(X_consensus_family_dummy, combination_dataset_habitat_type, sum_threshold, habitats_larvae + habitats_adults)

title_string = f"Histogram of Habitats of Larvae and Adults for Consensus Families along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=2, cols=1)
for habitat_type in habitats_larvae:

    subplot = go.Bar(x=X_habitat_types["consensus_family"],
                        y=X_habitat_types[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

for habitat_type in habitats_adults:

    subplot = go.Bar(x=X_habitat_types["consensus_family"],
                        y=X_habitat_types[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 2, col = 1)

fig.update_xaxes(tickangle= -45)  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_habitat_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%
# %%
# %% Histogram of habitats of larvae and adults for Observations along Combination of Dataset and Habitat Type
combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

habitats_larvae = ["larva_terrestrial", "larva_aquatic"]
habitats_adults = ["adult_terrestrial", "adult_aquatic"]

X_habitat_types = get_insect_trait_metadata_along_observations(X_consensus_family_dummy, combination_dataset_habitat_type,  habitats_larvae + habitats_adults)

title_string = f"Histogram of Habitats of Larvae and Adults for Observations along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=2, cols=1)
for habitat_type in habitats_larvae:

    subplot = go.Bar(x=X_habitat_types["observation_id"], 
                        y=X_habitat_types[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

for habitat_type in habitats_adults:

    subplot = go.Bar(x=X_habitat_types["observation_id"], 
                        y=X_habitat_types[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 2, col = 1)

fig.update_xaxes(tickangle= -45)  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_habitat_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_larvae for Consensus Family along Combination of Dataset and Habitat Type

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

sum_threshold = 500

diet_types_of_larvae = ["phytophagous", "zoophagous", "mycetophagous", "saprophagous", "detritophagous", "coprophagous"]

X_diet_types = get_insect_trait_metadata_along_consensus_families(X_consensus_family_dummy, combination_dataset_habitat_type, sum_threshold, diet_types_of_larvae)

title_string = f"Histogram of Diet Types of Larvae for Consensus Families along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_larvae:

    subplot = go.Bar(x=X_diet_types["consensus_family"], 
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_xaxes(tickangle= -45)  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_larvae for Observations along Combination of Dataset and Habitat Type
combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

diet_types_of_larvae = ["phytophagous", "zoophagous", "mycetophagous", "saprophagous", "detritophagous", "coprophagous"]

X_diet_types = get_insect_trait_metadata_along_observations(X_consensus_family_dummy, combination_dataset_habitat_type, diet_types_of_larvae)

title_string = f"Histogram of Diet Types of Larvae for Observations along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_larvae:

    subplot = go.Bar(x=X_diet_types["observation_id"], 
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_xaxes(tickangle= -45, showticklabels = True, type = "category")  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_phytophagous_larvae for Consensus Family along Combination of Dataset and Habitat Type

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

sum_threshold = 500

diet_types_of_phytophagous_larvae = ["phyllophagous", "saproxylic", "sap-sucking", "stem", "flower", "seed", "gall-inducing", "miners", "roots", "phytophagous not specified"]
X_diet_types = get_insect_trait_metadata_along_consensus_families(X_consensus_family_dummy, 
                                                                    combination_dataset_habitat_type, 
                                                                    sum_threshold, 
                                                                    diet_types_of_phytophagous_larvae,
                                                                    "phytophagous")

title_string = f"Histogram of Diet Types of Phytophagous Larvae for Consensus Families along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_phytophagous_larvae:

    subplot = go.Bar(x=X_diet_types["consensus_family"],  
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_xaxes(tickangle= -45)  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_phytophagous_larvae for Observations along Combination of Dataset and Habitat Type
combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

extra_factor = "phytophagous"

diet_types_of_phytophagous_larvae = ["phyllophagous", "saproxylic", "sap-sucking", "stem", "flower", "seed", "gall-inducing", "miners", "roots", "phytophagous not specified"]
X_diet_types = get_insect_trait_metadata_along_observations(X_consensus_family_dummy, combination_dataset_habitat_type,  diet_types_of_phytophagous_larvae, extra_factor)

title_string = f"Histogram of Diet Types of Phytophagous Larvae for Observations along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_phytophagous_larvae:

    subplot = go.Bar(x=X_diet_types["observation_id"], 
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_xaxes(tickangle= -45, showticklabels = True, type = "category")  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_zoophagous_larvae for Consensus Family along Combination of Dataset and Habitat Type

combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

sum_threshold = 500

diet_types_of_zoophagous_larvae = ["predator", "micropredator", "parasite", "parasitoid", "necrophorous", "zoophagous not specified"]

X_diet_types = get_insect_trait_metadata_along_consensus_families(X_consensus_family_dummy, 
                                                                    combination_dataset_habitat_type, 
                                                                    sum_threshold, 
                                                                    diet_types_of_zoophagous_larvae,
                                                                    "zoophagous")

title_string = f"Histogram of Diet Types of Zoophagous Larvae for Consensus Families along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_zoophagous_larvae:

    subplot = go.Bar(x=X_diet_types["consensus_family"],  
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)  

fig.update_layout(title_text=title_string, barmode='stack')
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Histogram of diet_types_of_zoophagous_larvae for Observations along Combination of Dataset and Habitat Type
combination_dataset_habitat_type = [("LandKLIF", "forest"), ("NatWald", "forest")]
combination_dataset_habitat_type = [("LfU_AUM_2022", "agriculture")]

extra_factor = "zoophagous"

diet_types_of_zoophagous_larvae = ["predator", "micropredator", "parasite", "parasitoid", "necrophorous", "zoophagous not specified"]
X_diet_types = get_insect_trait_metadata_along_observations(X_consensus_family_dummy, combination_dataset_habitat_type,  diet_types_of_zoophagous_larvae, extra_factor)

title_string = f"Histogram of Diet Types of Zoophagous Larvae for Observations along Combination of Dataset and Habitat Type {str(combination_dataset_habitat_type)}"
fig = make_subplots(rows=1, cols=1)
for diet_type in diet_types_of_zoophagous_larvae:

    subplot = go.Bar(x=X_diet_types["observation_id"], 
                        y=X_diet_types[diet_type], 
                        name=diet_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_xaxes(tickangle= -45, showticklabels = True, type = "category")  
fig.update_layout(title_text=title_string, barmode='stack')

Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")
Load_Store_Data().saving(X_diet_types, store_img_data_folder, title_string, "csv", float_format='%.2f')

# %%

# %% Plot WordClouds
from wordcloud import WordCloud

habitat_type = "forest"
dataset = "All"

sum_threshold = 0

mask = ((df_target["dataset"] == dataset )
                & (df_target["habitat_type"] == habitat_type))
mask = df_target["habitat_type"] == habitat_type
X_consensus_family = (pd.DataFrame(np.sum(X_raw_dummy[mask],axis=0), columns=["counts"]).reset_index().rename({"index" : "bold_bin_uri"}, axis=1)
                        .merge(df_bin_consensus_family_connection, on='bold_bin_uri')[["consensus_family", "counts"]]
                        .groupby("consensus_family").sum().reset_index())
X_consensus_family = X_consensus_family[X_consensus_family["counts"] > sum_threshold]
X_consensus_family_as_dict = {key : val for key, val in zip(X_consensus_family["consensus_family"].astype("string"), 
                                                                X_consensus_family["counts"].astype("int"))}

wordcloud = WordCloud().generate_from_frequencies(X_consensus_family_as_dict)

data_folder = "plots/FeaturesDistribution/WordClouds/"
filename = f"WordCloud_{dataset}_{habitat_type}"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "svg"))

with open(filepath,"w+") as f:
    f.write(wordcloud.to_svg(embed_font=True))

# %%

# %%
# %% Distribution of top consensus families along habitat types

top_n = 15

habitat_types = ["agriculture", "forest", "nature",  "urban"]

X_plot = {}
for habitat_type in habitat_types:
    mask = df_target["habitat_type"] == habitat_type

    top_n_families = np.sum(X_consensus_family_dummy[mask],axis=0).sort_values()[-top_n:]

    X_plot[habitat_type] = pd.DataFrame(np.sum(X_consensus_family_dummy[mask],axis=0).sort_values()[-top_n:], columns = [habitat_type])

X_plot = pd.concat([X_plot[habitat_type] for habitat_type in habitat_types], axis = 1).fillna(0)

title_string = f"Absolut Distribution of Top {top_n} Consensus Families along Habitat Types"
fig = make_subplots(rows=1, cols=1)
for habitat_type in habitat_types:
    subplot = go.Bar(x=X_plot[habitat_type].index, 
                        y=X_plot[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)
    #fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")

# %%
np.sum(threshold_families_normalized, axis=0)

# %% Distribution of top consensus families along habitat types

threshold = 0.01

habitat_types = ["agriculture", "forest", "nature",  "urban"]

X_plot = {}
for habitat_type in habitat_types:
    mask = df_target["habitat_type"] == habitat_type

    threshold_families = np.sum(X_consensus_family_normalized[mask], axis=0) >= threshold * len(X_consensus_family_normalized[mask])

    threshold_families_normalized = X_consensus_family_normalized.loc[mask, threshold_families].apply(lambda x : x / np.sum(X_consensus_family_normalized.loc[mask, threshold_families], axis=1))
    
    X_plot[habitat_type] = pd.DataFrame(np.sum(threshold_families_normalized, axis=0) / np.sum(threshold_families_normalized, axis=0).sum(), columns = [habitat_type])

X_plot = pd.concat([X_plot[habitat_type] for habitat_type in habitat_types], axis = 1).fillna(0)

title_string = f"Normalized Distribution of Consensus Families with min. {threshold} cover quota along Habitat Types"
fig = make_subplots(rows=1, cols=1)
for habitat_type in habitat_types:
    subplot = go.Bar(x=X_plot[habitat_type].index, 
                        y=X_plot[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)
    #fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")

# %%
# %% Distribution of top consensus families along months

top_n = 15

months = [date.month_name() for date in sorted(set(df_target["month"]))]

X_plot = {}
for month in months:
    mask = df_target["month"].dt.month_name() == month
    
    top_n_families = np.sum(X_consensus_family_dummy[mask],axis=0).sort_values()[-top_n:]

    X_plot[month] = pd.DataFrame(np.sum(X_consensus_family_dummy[mask],axis=0).sort_values()[-top_n:], columns = [month])

X_plot = pd.concat([X_plot[month] for month in months], axis = 1).fillna(0)

title_string = f" Absolut Distribution of Top {top_n} Consensus Families along Months"
fig = make_subplots(rows=1, cols=1)
for month in months:
    subplot = go.Bar(x=X_plot[month].index, 
                        y=X_plot[month], 
                        name=month,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)
    #fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(months)})", "html")

# %%

# %% Distribution of top consensus families along months

top_n = 15

months = [date.month_name() for date in sorted(set(df_target["month"]))]

X_plot = {}
for month in months:
    mask = df_target["month"].dt.month_name() == month
    
    threshold_families = np.sum(X_consensus_family_normalized[mask], axis=0) >= threshold * len(X_consensus_family_normalized[mask])

    threshold_families_normalized = X_consensus_family_normalized.loc[mask, threshold_families].apply(lambda x : x / np.sum(X_consensus_family_normalized.loc[mask, threshold_families], axis=1))
    
    X_plot[month] = pd.DataFrame(np.sum(threshold_families_normalized, axis=0) / np.sum(threshold_families_normalized, axis=0).sum(), columns = [month])

X_plot = pd.concat([X_plot[month] for month in months], axis = 1).fillna(0)

title_string = f"Normalized Distribution of Consensus Families with min. {threshold} cover quota along Months"
fig = make_subplots(rows=1, cols=1)
for month in months:
    subplot = go.Bar(x=X_plot[month].index, 
                        y=X_plot[month], 
                        name=month,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(tickangle= -45)
    #fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(months)})", "html")

# %%
# %% Distribution of top BOLD BIN along habitat types

threshold = 0.10
min_counts_for_threshold_reached = 0.1

habitat_types = ["agriculture", "forest", "nature",  "urban"]

X_plot = {}
for habitat_type in habitat_types:
    
    habitat_mask = df_target["habitat_type"] == habitat_type

    threshold_column = threshold * np.sum(X_raw[habitat_mask], axis = 1)
    threshold_mask = X_raw[habitat_mask].apply(lambda x : x >= threshold_column)

    counts_mask = np.sum(threshold_mask, axis = 0) > min_counts_for_threshold_reached * len(threshold_mask)

    X_plot[habitat_type] = pd.DataFrame(np.sum(threshold_mask.loc[:,counts_mask], axis=0) / len(threshold_mask), columns=[habitat_type])

X_plot = pd.concat([X_plot[habitat_type] for habitat_type in habitat_types], axis = 1).fillna(0)

title_string = f"Distribution of BOLD BINs covering {threshold} % of all OTUs throughout min. {min_counts_for_threshold_reached} % of observations along Habitat Types"
fig = make_subplots(rows=1, cols=1)
for habitat_type in habitat_types:
    subplot = go.Bar(x=X_plot[habitat_type].index, 
                        y=X_plot[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_xaxes(tickangle= -45)
fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")

# %%

# %% Distribution of top BOLD BIN along habitat types

threshold = 0.01

habitat_types = ["agriculture", "forest", "nature",  "urban"]

X_plot = {}
for habitat_type in habitat_types:
    mask = df_target["habitat_type"] == habitat_type
    X_observations_counts = np.sum(X_raw[mask], axis = 1)
    X_normalized = X_raw[mask].apply(lambda x : x / X_observations_counts)
    X_bins_covering = ( np.sum(X_normalized, axis = 0) / np.sum(X_normalized, axis = 0).sum() )
    threshold_mask = X_bins_covering > threshold
    X_plot[habitat_type] = pd.DataFrame(X_bins_covering[threshold_mask], columns=[habitat_type])

X_plot = pd.concat([X_plot[habitat_type] for habitat_type in habitat_types], axis = 1).fillna(0)

title_string = f"Distribution of BOLD BINs covering {threshold} % of all OTUs over all observations along Habitat Types"
fig = make_subplots(rows=1, cols=1)
for habitat_type in habitat_types:
    subplot = go.Bar(x=X_plot[habitat_type].index, 
                        y=X_plot[habitat_type], 
                        name=habitat_type,
                        )
    fig.add_trace(subplot, row = 1, col = 1)
    fig.update_xaxes(categoryorder='array', categoryarray= sorted(X_plot.index))

fig.update_xaxes(tickangle= -45)
fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string + f" ({str(habitat_types)})", "html")
# %%

# %%

# %%
def get_top_n_features(top_n, df):
    top_n_features = pd.DataFrame({n: df.T[col].nlargest(top_n).index.tolist() for n, col in enumerate(df.T)}).T
    top_n_features.set_index(df.index, inplace=True)

    top_n_features_values = pd.DataFrame(columns=top_n_features.columns, index = top_n_features.index)
    for i in top_n_features.index:
        for col in top_n_features:
            top_n_features_values.loc[i, col] = df.loc[i, top_n_features.loc[i, col]]

    top_n_features_summed_up = np.sum(top_n_features_values, axis=1)

    top_n_features_list = set([item for sublist in [list(top_n_features[col].ravel()) for col in top_n_features] for item in sublist])

    return top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list

# %%
top_n = 10
X_fraction = X_raw.divide(np.sum(X_raw, axis=1), axis = "index")

top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list = get_top_n_features(top_n, X_fraction)


# %%
X_fraction.info

# %% Distribution of top ten features for each observation
title_string = "Distribution of top ten features for each observation"

fig = plx.scatter(top_n_features_sum,
    facet_col=df_target["habitat_type"],
    color = df_target["dataset"],
    hover_data={"dataset": df_target["dataset"]},
    labels={"color" : "habitat_type"},
    title = title_string
)

data_folder = "plots/FeaturesDistribution/"
Load_Store_Data().saving(fig, data_folder, title_string, "html")

# %% Distribution of top features summed up
title_string = "Distribution of top features summed up"

fig = plx.scatter(np.sum(X_fraction[top_n_features_list], axis=0), 
    title = title_string
)

data_folder = "plots/FeaturesDistribution/"
Load_Store_Data().saving(fig, data_folder, title_string, "html")

# %%
df_target["dataset"].value_counts()

# %% Distribution of top features for datasets
datasets = ["LandKLIF", "LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021", "NatWald"]

for filename in datasets:

    mask = df_target["dataset"] == filename

    top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list = get_top_n_features(top_n, X_fraction[mask])

    title_string = f"Distribution of top features for {filename} summed up"

    fig = plx.scatter(np.sum(X_fraction.loc[mask, top_n_features_list], axis=0),      
        title = title_string
    )
    fig.update_yaxes(range=[0, 1])

    data_folder = f"plots/FeaturesDistribution/{filename}/"
    Load_Store_Data().saving(fig, data_folder, title_string, "html")


    title_string = f"Distribution of top features for {filename} for each observation"

    fig = plx.scatter(top_n_features_sum,
        title = title_string
    )
    fig.update_yaxes(range=[0, 1])

    Load_Store_Data().saving(fig, data_folder, title_string, "html")

# %%


# %% Mean and std of top features for datasets
datasets = ["LandKLIF", "LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021", "NatWald"]
#datasets = ["LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2020", "NatWald"]

for filename in datasets:

    mask = df_target["dataset"] == filename

    top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list = get_top_n_features(top_n, X_fraction[mask])
   
    print(filename)
    print(top_n_features_sum.describe())


# %%

# %% Distribution of top features for test cases

test_datasets = ["Braunisch", "Heibl", "Eutropia", "Kaczmarek", "Lobinger"]
target_headers = ["sum_raw_reads", "customer_sampleID"]

for filename in test_datasets:

    data_folder = f"data/TestData/TestCases"

    df_test = load_store_data.read_raw_parquet(data_folder, filename)
    bin_columns=[col for col in df_test if not col in target_headers]
    X_test = df_test[bin_columns]
    X_test_frac = X_test.divide(np.sum(X_test, axis=1), axis = "index")

    top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list = get_top_n_features(top_n, X_test_frac)


    title_string = f"Distribution of top features for {filename} summed up"

    fig = plx.scatter(np.sum(X_test_frac[top_n_features_list], axis=0),      
        title = title_string
    )
    fig.update_yaxes(range=[0, 1])

    data_folder = "plots/FeaturesDistribution/TestCases/"
    Load_Store_Data().saving(fig, data_folder, title_string, "html")


    title_string = f"Distribution of top features for {filename} for each observation"

    fig = plx.scatter(top_n_features_sum,
        title = title_string
    )
    fig.update_yaxes(range=[0, 1])

    Load_Store_Data().saving(fig, data_folder, title_string, "html")


# %% Mean and std of top features for test cases
test_datasets = ["Braunisch", "Heibl", "Eutropia", "Kaczmarek", "Lobinger"]

target_headers = ["sum_raw_reads", "customer_sampleID", "extra_target"]

top_n = 5

for filename in test_datasets:

    data_folder = f"data/TestData/TestCases"

    df_test = Load_Store_Data().loading(data_folder, filename, "parquet")

    bin_columns=[col for col in df_test if not col in target_headers]

    X_train_scaled = df_test[bin_columns].divide(np.sum(df_test[bin_columns], axis=1), axis = "index")

    top_n_features, top_n_features_values, top_n_features_sum, top_n_features_list = get_top_n_features(top_n, X_train_scaled)
   
    print(filename)
    print(top_n_features_sum.describe())


# %%
