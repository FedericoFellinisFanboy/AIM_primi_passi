from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)

app.config['SECRET_KEY'] = 'IVAmt7bQtOUYQLJGfs5D1J7Ds2iup7Gl' # https://randomkeygen.com/

@app.route('/')
def hello():
    return render_template('index.html', utc_dt="1234432")