# %% Import used packages
import pandas as pd
import numpy as np

# import geopandas
import joblib
import plotly.express as plx
import plotly.graph_objects as go
import os

from sklearn.base import is_classifier, is_regressor
#from xgboost import XGBClassifier


class Load_Store_Data: 
    """Have all data loading and storage shizzle combined in one class
    """
    def __init__(self):
        # Set the path to the project folder
        self.project_folder = "~/Code/AIM_primi_passi/"

    def get_path(self, _data_folder : str, _filename : str, _file_extension : str, _project_folder : str = None):

        if _project_folder is None:
            path = self.project_folder if self.project_folder[-1] == "/" else f"{self.project_folder}/"
        else:
            path = _project_folder if _project_folder[-1] == "/" else f"{_project_folder}/"

        path += _data_folder if _data_folder[-1] == "/" else f"{_data_folder}/"
        path += f"{_filename}{_file_extension}" if _file_extension[0] == "." else f"{_filename}.{_file_extension}"

        return os.path.expanduser(path)

    # %% Loading the data depending on its file extension
    def loading(self, _data_folder : str, _filename : str, _file_extension : str, _project_folder : str = None , **kwargs):

        filepath = self.get_path(_data_folder, _filename, _file_extension, _project_folder)

        nothing_loaded = "Unkown type. Nothing was loaded."

        if "csv" in _file_extension :
            return self.__read_raw_csv(filepath, **kwargs)

        elif "tsv" in _file_extension:
            return self.__read_raw_tsv(filepath, **kwargs)

        elif "parquet" in _file_extension:
            return self.__read_raw_parquet(filepath, **kwargs)
            
        elif "joblib" in _file_extension:
            return self.__read_joblib(filepath, **kwargs)

        elif "json" in _file_extension:
            if 'xgb_classifier' in kwargs and xgb_classifier:
                xgb_model = XGBClassifier()
                return xgb_model.load_model(filepath, **kwargs)

        else:
            print(nothing_loaded)

    # %% Save the data depending on its type
    def saving(self, _object_for_storage, _data_folder : str, _filename : str, _file_extension : str, _project_folder : str = None , **kwargs):

        filepath = self.get_path(_data_folder, _filename, _file_extension, _project_folder)

        nothing_saved = "Unkown type. Nothing was saved."

        if isinstance(_object_for_storage, go.Figure):
            self.__write_img_to(_object_for_storage, filepath, _file_extension, **kwargs)

        elif is_classifier(_object_for_storage) | is_regressor(_object_for_storage):
            self.__write_model_to(_object_for_storage, filepath, **kwargs)

        elif isinstance(_object_for_storage, pd.DataFrame):
            if "parquet" in _file_extension:
                self.__write_parquet(_object_for_storage, filepath, **kwargs)
            elif "csv" in _file_extension:
                self.__write_raw_csv(_object_for_storage, filepath, **kwargs)
            else:
                print(nothing_saved)
        else:
            print(nothing_saved)

    # %% Load the data as raw csv
    def __read_raw_csv(self, _filepath, **kwargs):

        try:
            return pd.read_csv(_filepath,
                                delimiter = ";",
                                low_memory= False,
                                encoding="utf-8",
                                **kwargs
                            ).dropna(how="all")
        except UnicodeDecodeError:
            return pd.read_csv(_filepath,
                                delimiter = ";",
                                low_memory= False,
                                encoding='latin-1',
                                **kwargs
                            ).dropna(how="all")

    # %% Load the data as raw tsv
    def __read_raw_tsv(self, _filepath, **kwargs):

        try:
            return pd.read_csv(_filepath,
                                delimiter = "\t",
                                low_memory= False,
                                encoding="utf-8",
                                **kwargs
                            ).dropna(how="all")
        except UnicodeDecodeError:
            return pd.read_csv(_filepath,
                                delimiter = "\t",
                                low_memory= False,
                                encoding='latin-1',
                                **kwargs
                            ).dropna(how="all")

    # %% Load the data
    def __read_raw_parquet(self, _filepath, **kwargs):

        try:
            return pd.read_parquet(_filepath, **kwargs)
        except:
            try:
                self.__read_geo_parquet(_filepath, **kwargs)
            except:
                pass

    # %% Load data with geo information
    def __read_geo_parquet(self, _filepath, **kwargs):

        return geopandas.read_parquet(_filepath, **kwargs)

    # %% Load model
    def __read_joblib(self, _filepath, **kwargs):

        return joblib.load(_filepath, **kwargs)

    # %% Save the data as parquet file
    def __write_parquet(self, _df, _filepath, **kwargs):

        _df.to_parquet(_filepath, **kwargs)
        # try:
        #     _df.to_parquet(_filepath, **kwargs)
        # except EOFError():
        #     return EOFError()

    # %% Save the data as csv file
    def __write_raw_csv(self, _df, _filepath, **kwargs):

        try:
            _df.to_csv(_filepath, sep = ";", index=False, **kwargs)
        except EOFError():
            return EOFError()

    # %% Save the img to file
    def __write_img_to(self, _fig, _filepath, _file_extension, **kwargs):
        
        if _file_extension == "html":
            _fig.write_html(_filepath)
        else:
            _fig.write_image(_filepath)

    # %% Save the model to joblib or sth else
    def __write_model_to(self, _model, _filepath, **kwargs):
        
        try:
            joblib.dump(_model , _filepath)
        except:
            pass