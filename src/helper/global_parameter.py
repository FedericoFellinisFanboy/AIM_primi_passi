# %% Import used packages
import pandas as pd
import numpy as np

# from geopy.geocoders import Nominatim
# from geopy.extra.rate_limiter import RateLimiter

from load_store_data import Load_Store_Data

# %%
timestamp_GTS_per_year = {
    "1900" : pd.to_datetime("1900/01/01"),
    "2018" : pd.to_datetime("2018/04/03"),
    "2019" : pd.to_datetime("2019/03/14"),
    "2020" : pd.to_datetime("2020/03/06"),
    "2021" : pd.to_datetime("2021/03/25")
}

# %%
since_gts_intervall = 30

# %%
consensus_score_threshold = {"A", "B", "yes"}

# %%
consensus_taxon_grades = ['consensus_domain', 
                            'consensus_phylum',
                            'consensus_class',
                            'consensus_order',
                            'consensus_family',
                            'consensus_genus',
                            'consensus_species']