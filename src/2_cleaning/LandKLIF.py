# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/LandKLIF/COMBINATION 2019-2021/"
filename = "Landklif_custom_results_names_JM_rdy"
df_LandKLIF = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Load meta data
data_folder = "data/LandKLIF/COMBINATION 2019-2021/MetaData/"
filename = "LandKlif_sample_info_revised_kl"
df_LandKLIF_metadata = Load_Store_Data().loading(data_folder, filename, "csv")

df_LandKLIF_metadata["Visit"] = df_LandKLIF_metadata["Visit"].astype(str)

filename = "LANDKLIF-clustering-lisi-5"
df_LandKLIF_metadata_newTargets_F_U = Load_Store_Data().loading(data_folder, filename, "csv")

filename = "LANDKLIF-clustering – Emelö – neu"
df_LandKLIF_metadata_newTargets_A_G = Load_Store_Data().loading(data_folder, filename, "csv")

filename = "Abkürzungen (Erklärung)"
df_LandKLIF_metadata_newTargets_abbr_infos = Load_Store_Data().loading(data_folder, filename, "csv").T[0].to_dict()

# %% Get index for broadleaf forest, conifere forest and forest besides
mask_forest = df_LandKLIF_metadata_newTargets_F_U["PLOTID"].str.contains("_F")
df_LandKLIF_metadata_newTargets_forest = df_LandKLIF_metadata_newTargets_F_U[mask_forest]

forest_subtypes_distributions = [{subtypes[:len(abbr)] : int(subtypes[len(abbr):]) if subtypes[len(abbr):] else 0 
                                    for abbr, subtypes in zip(df_LandKLIF_metadata_newTargets_abbr_infos.values(), row.split("_"))}
                                    for row in df_LandKLIF_metadata_newTargets_forest["100"]]

mask_CF = pd.Series([subtype["CF"] > 85 for subtype in forest_subtypes_distributions],
                    index=df_LandKLIF_metadata_newTargets_forest.index)
quadrant_CF = df_LandKLIF_metadata_newTargets_forest.loc[mask_CF, "PLOTID"].str[:-2]


besides_BL_CF = set(df_LandKLIF_metadata_newTargets_abbr_infos.values()).difference(set(["CF", "BL"]))

mask_besides_BL_CF = pd.Series([any(subtype[land_type] > 0 
                                    for land_type in besides_BL_CF) for subtype in forest_subtypes_distributions],
                                    index=df_LandKLIF_metadata_newTargets_forest.index)

mask_besides_BL_CF &= ~mask_CF
quadrant_besides_BL_CF = df_LandKLIF_metadata_newTargets_forest.loc[mask_besides_BL_CF, "PLOTID"].str[:-2]

mask_BL = ~mask_CF & ~mask_besides_BL_CF
quadrant_BL = df_LandKLIF_metadata_newTargets_forest.loc[mask_BL, "PLOTID"].str[:-2]


# %% Get index for annual_crop_rape, annual_crop_others, agriculture besides

df_LandKLIF_metadata_newTargets_abbr_infos_agriculture = df_LandKLIF_metadata_newTargets_abbr_infos
df_LandKLIF_metadata_newTargets_abbr_infos_agriculture["pasture"] = "HPT"

mask_agriculture = df_LandKLIF_metadata_newTargets_A_G["PLOTID"].str.contains("_A")
df_LandKLIF_metadata_newTargets_agriculture = df_LandKLIF_metadata_newTargets_A_G[mask_agriculture]

agriculture_subtypes_distributions = [{subtypes[:len(abbr)] : int(subtypes[len(abbr):]) if subtypes[len(abbr):] else 0 
                                    for abbr, subtypes in zip(df_LandKLIF_metadata_newTargets_abbr_infos_agriculture.values(), row.split("_"))}
                                    for row in df_LandKLIF_metadata_newTargets_agriculture["100"]]

mask_ACO = pd.Series([subtype["ACO"] == max(subtype.values()) for subtype in agriculture_subtypes_distributions],
                    index=df_LandKLIF_metadata_newTargets_agriculture.index)
quadrant_ACO = df_LandKLIF_metadata_newTargets_agriculture.loc[mask_ACO, "PLOTID"].str[:-2]

mask_ACR = pd.Series([subtype["ACR"] == max(subtype.values()) for subtype in agriculture_subtypes_distributions],
                    index=df_LandKLIF_metadata_newTargets_agriculture.index)
quadrant_ACR = df_LandKLIF_metadata_newTargets_agriculture.loc[mask_ACR, "PLOTID"].str[:-2]

mask_besides_ACO_ACR = ~mask_ACO & ~mask_ACR
quadrant_besides_ACO_ACR = df_LandKLIF_metadata_newTargets_agriculture.loc[mask_besides_ACO_ACR, "PLOTID"].str[:-2]

arg = pd.DataFrame(agriculture_subtypes_distributions, index = df_LandKLIF_metadata_newTargets_agriculture.index)[mask_besides_ACO_ACR]
# %%
df_LandKLIF_metadata_newTargets_agriculture[mask_ACO | mask_ACR | mask_besides_ACO_ACR].index

# %%
# %% Set up mapping for habitat abbreviations to fully named habitat types
df_landKLIF_habitat_mapping = {"Ag" : "arable field",
                                            "AG" : "arable field",
                                            "For" : "forest",
                                            "Gra" : "meadow",
                                            "Urb" : "settlement",
                                            "Nat" : "nature"}

df_landKLIF_habitat_mapping_abbreviations = {"A" : "arable field",
                                                        "F" : "forest",
                                                        "G" : "meadow",
                                                        "U" : "settlement",
                                                        "N" : "nature"}

# %% Condense data information out of customer_sampleID columns to get habitat types

df_LandKLIF_temp = pd.DataFrame(df_LandKLIF["customer_sampleID"].str.replace(" ", "_"))
df_LandKLIF_temp["QuadrantID"] = df_LandKLIF_temp["customer_sampleID"].str[:6]

mask = df_LandKLIF_temp["customer_sampleID"].str.contains("_V")
df_LandKLIF_temp.loc[mask, "Visit"] = [sampleID_part[1:] for sampleID in df_LandKLIF_temp.loc[mask, "customer_sampleID"] for sampleID_part in sampleID.split("_") if "V" in sampleID_part]

sampleID_splitted = df_LandKLIF_temp["customer_sampleID"].str[6:].str.split("_")
mask = [bool(set(sampleID_part).intersection(set(df_landKLIF_habitat_mapping))) for sampleID_part in [sample_ID[:-1] for sample_ID in sampleID_splitted]]
df_LandKLIF_temp.loc[mask, "Habitat_type"] = [df_landKLIF_habitat_mapping[key] for sampleID_parts in sampleID_splitted for key in sampleID_parts if key in df_landKLIF_habitat_mapping]

mask = [bool(set(id for id in sampleID_part).intersection(df_landKLIF_habitat_mapping_abbreviations))  for sampleID_part in [sampleID[:-1] for sampleID in sampleID_splitted]]
sampleID_splitted_abbreviation = df_LandKLIF_temp.loc[mask, "customer_sampleID"].str[6:].str.split("_")
df_LandKLIF_temp.loc[mask, "Habitat_type"] = [df_landKLIF_habitat_mapping_abbreviations[key] 
                                                for sampleID_parts in sampleID_splitted_abbreviation
                                                for key in sampleID_parts if key in df_landKLIF_habitat_mapping_abbreviations]

# %% Some Visit values are missing and need to be added manually

visit_value_to_add = "10"

mask = df_LandKLIF_temp["Visit"].isna()

for quadrantID in df_LandKLIF_temp.loc[mask, "QuadrantID"].value_counts().index:

    submask = mask & (df_LandKLIF_temp["QuadrantID"] == quadrantID)

    habitat_type_count = len(set(df_LandKLIF_temp.loc[submask, "Habitat_type"]))

    # The observations are either be 5 times taken (G, K!, K2, K3, K4) or just 2 times (G, K)
    observations_grosz_klein1234_count = int(df_LandKLIF_temp.loc[submask, "QuadrantID"].value_counts() // 5)
    observations_grosz_klein_count = int( ( df_LandKLIF_temp.loc[submask, "QuadrantID"].value_counts() - (observations_grosz_klein1234_count * 5) ) // 2)

    if ( (habitat_type_count == observations_grosz_klein1234_count +  observations_grosz_klein_count)
        & (int(df_LandKLIF_temp.loc[submask, "QuadrantID"].value_counts()) == observations_grosz_klein1234_count * 5 +  observations_grosz_klein_count * 2) ):

        df_LandKLIF_temp.loc[submask, "Visit"] = visit_value_to_add

    elif habitat_type_count == int( df_LandKLIF_temp.loc[submask, "QuadrantID"].value_counts() // 2 ):

        df_LandKLIF_temp.loc[submask, "Visit"] = visit_value_to_add

    else:

        print(f"Check for {quadrantID} as there may be no correct visit nr. associatied:")

# %% Merge LandKLIF metadata via customer_sampleID infos
df_LandKLIF_temp_merged = pd.merge(df_LandKLIF_temp, df_LandKLIF_metadata[["QuadrantID", "Visit", "Habitat_type", "Date start", "Date end", "Lat", "Lon"]],  
                                    how='left', 
                                    on=["QuadrantID", "Visit", "Habitat_type"]
                                ).reset_index(drop=True)

# %%
df_LandKLIF["habitat_type"] = df_LandKLIF_temp_merged["Habitat_type"]

# # %% Get index to set broadleaf forest, conifere forest and forest besides values
# mask_forest_merge = df_LandKLIF_temp_merged["Habitat_type"] == "forest"

# mask_CF_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_CF) & mask_forest_merge
# mask_besides_BL_CF_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_besides_BL_CF) & mask_forest_merge
# mask_BL_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_BL) & mask_forest_merge

# df_LandKLIF.loc[mask_CF_merge, "habitat_type"] = "forest_CF"
# df_LandKLIF.loc[mask_besides_BL_CF_merge, "habitat_type"] = "forest_edge"
# df_LandKLIF.loc[mask_BL_merge, "habitat_type"] = "forest_BL"

# # %% Get index to set annual_crop_rape, annual_crop_others, agriculture besides
# mask_agriculture_merge = df_LandKLIF_temp_merged["Habitat_type"] == "arable field"

# mask_ACO_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_ACO) & mask_agriculture_merge
# mask_besides_ACO_ACR_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_besides_ACO_ACR) & mask_agriculture_merge
# mask_ACR_merge = df_LandKLIF_temp_merged["QuadrantID"].isin(quadrant_ACR) & mask_agriculture_merge

# df_LandKLIF.loc[mask_ACO_merge, "habitat_type"] = "agriculture_ACO"
# df_LandKLIF.loc[mask_besides_ACO_ACR_merge, "habitat_type"] = "agriculture_edge"
# df_LandKLIF.loc[mask_ACR_merge, "habitat_type"] = "agriculture_ACR"

# %%
df_LandKLIF["habitat_type"] = df_LandKLIF["habitat_type"].astype("category")

# %% Use the middle between start date and end date as timestamp
df_LandKLIF_temp_merged["timestamp"] = ( pd.to_datetime(df_LandKLIF_temp_merged["Date start"], format = "%d.%m.%Y")
                                        + pd.to_timedelta(
                                            (pd.to_datetime(df_LandKLIF_temp_merged["Date end"], format = "%d.%m.%Y") 
                                            - pd.to_datetime(df_LandKLIF_temp_merged["Date start"], format = "%d.%m.%Y")).dt.days / 2, 
                                            unit="day"
                                        ))

df_LandKLIF["timestamp"] = df_LandKLIF_temp_merged["timestamp"]

# %% Use Lon and Lat for geometry information

df_LandKLIF["latitude"] = df_LandKLIF_temp_merged["Lat"].str.replace(",", ".").astype("float").astype("category")
df_LandKLIF["longitude"] = df_LandKLIF_temp_merged["Lon"].str.replace(",", ".").astype("float").astype("category")

# df_LandKLIF = geopandas.GeoDataFrame(df_LandKLIF, 
#                                         geometry = geopandas.points_from_xy(df_LandKLIF_temp_merged["Lon"].str.replace(",", "."),
#                                                                             df_LandKLIF_temp_merged["Lat"].str.replace(",", ".")))

# %% Add observation_id
df_LandKLIF["dataset"] = "LandKLIF"
df_LandKLIF["dataset"] = df_LandKLIF["dataset"].astype("category", copy=False)

df_LandKLIF["observation_id"] = df_LandKLIF["customer_sampleID"]
# %% Remove customer_sampleID now that all data is extracted from it
df_LandKLIF.drop(columns=["customer_sampleID"], inplace = True)

# %% Store the cleaned data
data_folder = "data/LandKLIF/COMBINATION 2019-2021/"
filename = "Landklif_a_posto"
Load_Store_Data().saving(df_LandKLIF, data_folder, filename, "parquet")

# %%
X = pd.DataFrame(Load_Store_Data().loading(data_folder, filename, "parquet"))
# %%
X.columns
# %%
X.head()
# %%
X.info()
# %%
X["habitat_type"].value_counts()
# %%
X[X["habitat_type"] == "forest"]
# %%
