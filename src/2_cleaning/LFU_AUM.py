# %% Import used packages
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data


# %% Load the data
year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_combined_CE_results"
df_LFU_AUM_2019 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2020"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_2020"
df_LFU_AUM_2020 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
filename = "FT_Foeldesi_LFU_AUM_2021_pt3_results"
df_LFU_AUM_2021 = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "LFU_AUM_KF_results"
df_LFU_AUM_2021 = pd.concat([df_LFU_AUM_2021, Load_Store_Data().loading(data_folder, filename, "parquet")], ignore_index=True)

year = "2022"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_custom_2022_results_JM3"
df_LFU_AUM_2022 = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% pd.concat adds NaN which needs to be unified again
target_headers = ["customer_sampleID"]
bin_headers = [column for column in df_LFU_AUM_2021.columns if not column in target_headers]

df_LFU_AUM_2021.fillna({key : 0 for key in [column for column in df_LFU_AUM_2021.columns if not column in target_headers]}, inplace=True)
df_LFU_AUM_2021.max(numeric_only = True).max()
# %%
uint16_max_value = np.iinfo("uint16").max

if df_LFU_AUM_2021[bin_headers].max().max() <= np.iinfo("uint32").max:
    df_LFU_AUM_2021 = df_LFU_AUM_2021.astype({key : "uint16" if df_LFU_AUM_2021[key].max() <= uint16_max_value else "uint32" for key in bin_headers}, copy=False)
    df_LFU_AUM_2021 = df_LFU_AUM_2021.copy(deep=True)
else:
    print("OTC are too large for compressing data to uint16 or even uint32.")

# %% Load meta data
year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
data_folder += "MetaData/"

columns_metadata = ["Proben_ ID", "Flächen_ID", "Entleerung_Datum", "Durchgang_Nr", "XGrad", "YGrad"]

filename = "Data AIM Kescher"
df_LFU_AUM_metadata = Load_Store_Data().loading(data_folder, filename, "csv", usecols = columns_metadata)


columns_metadata += ["Typ"]
filename = "Data AIM Malaise"
df_LFU_AUM_metadata = pd.concat([df_LFU_AUM_metadata,
                            Load_Store_Data().loading(data_folder, filename, "csv", usecols = columns_metadata)],
                            ignore_index = True)

filename = "Data AIM Saug"
df_LFU_AUM_metadata = pd.concat([df_LFU_AUM_metadata,
                            Load_Store_Data().loading(data_folder, filename, "csv", usecols = columns_metadata)],
                            ignore_index = True)

# %% Use extra_target or customer sample ID to merge meta data
df_LFU_AUM_2019_temp = df_LFU_AUM_2019["extra_target"].str[:8].str.replace("_","")

df_LFU_AUM_2020_temp = df_LFU_AUM_2020["customer sample ID"].str[:8]

df_LFU_AUM_2021_temp = df_LFU_AUM_2021["customer_sampleID"]

df_LFU_AUM_2022_temp = df_LFU_AUM_2022["extra_target"]

# %% Merge meta data into observation data
df_LFU_AUM_2019_temp_merge = pd.merge(df_LFU_AUM_2019_temp, df_LFU_AUM_metadata,
                                        how="left",
                                        left_on="extra_target", right_on="Proben_ ID")

df_LFU_AUM_2020_temp_merge = pd.merge(df_LFU_AUM_2020_temp, df_LFU_AUM_metadata,
                                        how="left",
                                        left_on="customer sample ID", right_on="Proben_ ID")

df_LFU_AUM_2021_temp_merge = pd.merge(df_LFU_AUM_2021_temp, df_LFU_AUM_metadata,
                                        how="left",
                                        left_on="customer_sampleID", right_on="Proben_ ID")

df_LFU_AUM_2022_temp_merge = pd.merge(df_LFU_AUM_2022_temp, df_LFU_AUM_metadata,
                                        how="left",
                                        left_on="extra_target", right_on="Proben_ ID")

# %% Insert habitat type
df_LFU_AUM_2019["habitat_type"] = pd.Series(["agriculture"] * len(df_LFU_AUM_2019), dtype="category")
df_LFU_AUM_2020["habitat_type"] = pd.Series(["agriculture"] * len(df_LFU_AUM_2020), dtype="category")
df_LFU_AUM_2021["habitat_type"] = pd.Series(["agriculture"] * len(df_LFU_AUM_2021), dtype="category")
df_LFU_AUM_2022["habitat_type"] = pd.Series(["agriculture"] * len(df_LFU_AUM_2022), dtype="category")

# %% Insert trap type
trap_type_mapping = {
    "AMD" : "Malaise",
    "WMD" : "Malaise",
    "WKM" : "Kescher",
    "WSD" : "Saug",
}

df_LFU_AUM_2019["trap_type"] = df_LFU_AUM_2019["extra_target"].apply(lambda sampleID : trap_type_mapping[sampleID[:3]] if sampleID[:3] in trap_type_mapping else None).astype("category")
df_LFU_AUM_2020["trap_type"] = df_LFU_AUM_2020["customer sample ID"].apply(lambda sampleID : trap_type_mapping[sampleID[:3]] if sampleID[:3] in trap_type_mapping else None).astype("category")
df_LFU_AUM_2021["trap_type"] = df_LFU_AUM_2021["customer_sampleID"].apply(lambda sampleID : trap_type_mapping[sampleID[:3]] if sampleID[:3] in trap_type_mapping else None).astype("category")
df_LFU_AUM_2022["trap_type"] = df_LFU_AUM_2022["extra_target"].apply(lambda sampleID : trap_type_mapping[sampleID[:3]] if sampleID[:3] in trap_type_mapping else None).astype("category")

# %% Add timestamp to LfU
df_LFU_AUM_2019["timestamp"] = [pd.to_datetime(val, format = "%d.%m.%Y", errors = "coerce", exact=False) for val in df_LFU_AUM_2019_temp_merge["Entleerung_Datum"]]

df_LFU_AUM_2020["timestamp"] = [pd.to_datetime(val, format = "%d.%m.%Y", errors = "coerce", exact=False) for val in df_LFU_AUM_2020_temp_merge["Entleerung_Datum"]]

df_LFU_AUM_2021["timestamp"] = [pd.to_datetime(val, format = "%d.%m.%Y", errors = "coerce", exact=False) for val in df_LFU_AUM_2021_temp_merge["Entleerung_Datum"]]

df_LFU_AUM_2022["timestamp"] = [pd.to_datetime(val, format = "%d.%m.%Y", errors = "coerce", exact=False) for val in df_LFU_AUM_2022_temp_merge["Entleerung_Datum"]]

# %% Get Lon and Lat for geometry information via geopy
year = 2019
data_folder = f"data/LFU_AUM/{year}/"
data_folder += "MetaData/"
filename = "LFU_AUM_2019_geolocation"

load_geolocation_data_from_Nominatim = False

if load_geolocation_data_from_Nominatim:

    geolocator = Nominatim(user_agent="name-of-your-user-agent")
    geolocator_delayed = RateLimiter(geolocator.geocode, min_delay_seconds=1)

    df_locations_metadata_2019 = pd.DataFrame(list(set(df_LFU_AUM_2019_temp_merge["Ort"])), columns=["Geo_ID"])

    df_locations_metadata_2019["geolocator"] = df_locations_metadata_2019["Geo_ID"].apply(geolocator_delayed)

    df_locations_metadata_2019["address"] = df_locations_metadata_2019["geolocator"].apply(lambda x: x.address if x else None)
    df_locations_metadata_2019["latitude"] = df_locations_metadata_2019["geolocator"].apply(lambda x: x.latitude if x else None)
    df_locations_metadata_2019["longitude"] = df_locations_metadata_2019["geolocator"].apply(lambda x: x.longitude if x else None)
    
    Load_Store_Data().saving(df_locations_metadata_2019.drop(axis=1, columns=["geolocator"]), data_folder, filename, "csv")

    df_LFU_AUM_2019_temp_merge = pd.merge(df_LFU_AUM_2019_temp_merge["Ort"], df_locations_metadata_2019[["Geo_ID", "latitude", "longitude"]],  
                                    how='left', 
                                    left_on=["Ort"],
                                    right_on=["Geo_ID"]
                                ).reset_index(drop=True)

else:

    df_locations_metadata_2019 = Load_Store_Data().loading(data_folder, filename, "csv")

# %% Load LFU_AUM_2019 location metadata

# df_LFU_AUM_2019 = geopandas.GeoDataFrame(df_LFU_AUM_2019, 
#                                         geometry = geopandas.points_from_xy(df_LFU_AUM_2019_temp_merge["longitude"], 
#                                                                             df_LFU_AUM_2019_temp_merge["latitude"]))

df_LFU_AUM_2019["latitude"] = df_LFU_AUM_2019_temp_merge["YGrad"].astype("category")
df_LFU_AUM_2019["longitude"] = df_LFU_AUM_2019_temp_merge["XGrad"].astype("category")

# %% Use XGrad and YGrad for geometry information

# df_LFU_AUM_2020 = geopandas.GeoDataFrame(df_LFU_AUM_2020, 
#                                         geometry = geopandas.points_from_xy(df_LFU_AUM_2020_temp_merge["YGrad"], 
#                                                                             df_LFU_AUM_2020_temp_merge["XGrad"]))

df_LFU_AUM_2020["latitude"] = df_LFU_AUM_2020_temp_merge["YGrad"].astype("category")
df_LFU_AUM_2020["longitude"] = df_LFU_AUM_2020_temp_merge["XGrad"].astype("category")


# %% Use XGrad and YGrad for geometry information

# df_LFU_AUM_2021 = geopandas.GeoDataFrame(df_LFU_AUM_2021, 
#                                         geometry = geopandas.points_from_xy(df_LFU_AUM_2021_temp_merge["YGrad"], 
#                                                                             df_LFU_AUM_2021_temp_merge["XGrad"]))

df_LFU_AUM_2021["latitude"] = df_LFU_AUM_2021_temp_merge["YGrad"].astype("category")
df_LFU_AUM_2021["longitude"] = df_LFU_AUM_2021_temp_merge["XGrad"].astype("category")

# %% Use XGrad and YGrad for geometry information

df_LFU_AUM_2022["latitude"] = df_LFU_AUM_2022_temp_merge["YGrad"].astype("category")
df_LFU_AUM_2022["longitude"] = df_LFU_AUM_2022_temp_merge["XGrad"].astype("category")

# %% Use index as observation_id
df_LFU_AUM_2019["dataset"] = "LfU_AUM_2019"
df_LFU_AUM_2020["dataset"] = "LfU_AUM_2020"
df_LFU_AUM_2021["dataset"] = "LfU_AUM_2021"
df_LFU_AUM_2022["dataset"] = "LfU_AUM_2022"

df_LFU_AUM_2019["dataset"] = df_LFU_AUM_2019["dataset"].astype("category", copy=False)
df_LFU_AUM_2020["dataset"] = df_LFU_AUM_2020["dataset"].astype("category", copy=False)
df_LFU_AUM_2021["dataset"] = df_LFU_AUM_2021["dataset"].astype("category", copy=False)
df_LFU_AUM_2022["dataset"] = df_LFU_AUM_2022["dataset"].astype("category", copy=False)

df_LFU_AUM_2019["observation_id"] = df_LFU_AUM_2019["extra_target"]
df_LFU_AUM_2020["observation_id"] = df_LFU_AUM_2020["customer sample ID"]
df_LFU_AUM_2021["observation_id"] = df_LFU_AUM_2021["customer_sampleID"]
df_LFU_AUM_2022["observation_id"] = df_LFU_AUM_2022["extra_target"]

# %% Remove old target columns now that all data is extracted from them
df_LFU_AUM_2019.drop(axis=1, columns=["Date Malaise trap emptied =>", "Category =>", "extra_target"], inplace = True)
df_LFU_AUM_2020.drop(axis=1, columns=["customer sample ID"], inplace = True)
df_LFU_AUM_2021.drop(axis=1, columns=["customer_sampleID"], inplace = True)
df_LFU_AUM_2022.drop(axis=1, columns=["extra_target"], inplace = True)

# %% Store the cleaned data
year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_a_posto"
Load_Store_Data().saving(df_LFU_AUM_2019, data_folder, filename, "parquet")

year = "2020"
data_folder = f"data/LFU_AUM/{year}/"
Load_Store_Data().saving(df_LFU_AUM_2020, data_folder, filename, "parquet")

year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
Load_Store_Data().saving(df_LFU_AUM_2021, data_folder, filename, "parquet")

year = "2022"
data_folder = f"data/LFU_AUM/{year}/"
Load_Store_Data().saving(df_LFU_AUM_2022, data_folder, filename, "parquet")

# %%
year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
X = pd.DataFrame(Load_Store_Data().loading(data_folder, filename, "parquet"))
# %%
X.columns
# %%
X.head()
# %%
X.info()
# %%
X["timestamp"].value_counts()
# %%
df_LFU_AUM_2021["timestamp"].value_counts()
# %%
