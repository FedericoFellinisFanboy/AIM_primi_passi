# %% Import used packages
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/NatWald/"
filename = "Kuehbandner_NatWald100_pt1_results_JM"
df_NatWald = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Insert habitat type
df_NatWald["habitat_type"] = "forest"
df_NatWald["habitat_type"] = df_NatWald["habitat_type"].astype("category", copy=False)

# %% Add timestamp to NatWald 
year = 2021
day = 15
df_NatWald_Leerung_datetime = {"Januar" : "01",
                                "Februar" : "02",
                                "März" : "03",
                                "April" : "04",
                                "Mai" : "05",
                                "Juni" : "06",
                                "Juli" : "07",
                                "August" : "08",
                                "September" : "09",
                                "Oktober" : "10",
                                "November" : "11",
                                "Dezember" : "12"
}

df_NatWald["timestamp"] = df_NatWald["Leerung"].apply(lambda x : pd.to_datetime(f"{year}-{df_NatWald_Leerung_datetime[x]}-{day}"))

# %% Get Lon and Lat for geometry information via geopy

load_geolocation_data_from_Nominatim = False

if load_geolocation_data_from_Nominatim:
    geolocator = Nominatim(user_agent="name-of-your-user-agent")
    geolocator_delayed = RateLimiter(geolocator.geocode, min_delay_seconds=1)

    df_locations_metadata = pd.DataFrame(df_NatWald["Geo_ID"].cat.categories, columns=["Geo_ID"])
    
    df_locations_metadata["geolocator"] = df_locations_metadata["Geo_ID"].apply(geolocator_delayed)

    df_locations_metadata["address"] = df_locations_metadata["geolocator"].apply(lambda x: x.address if x else None)
    df_locations_metadata["latitude"] = df_locations_metadata["geolocator"].apply(lambda x: x.latitude if x else None)
    df_locations_metadata["longitude"] = df_locations_metadata["geolocator"].apply(lambda x: x.longitude if x else None)

    data_folder += "MetaData/"
    filename = "NatWald_geolocation"

    Load_Store_Data().saving(df_locations_metadata.drop(axis=1, columns=["geolocator"]), data_folder, filename, "csv")

else:
    data_folder += "MetaData/"
    filename = "NatWald_geolocation"

    df_locations_metadata = Load_Store_Data().loading(data_folder, filename, "csv")

# %% Load NatWald location metadata
df_NatWald_temp_merged = pd.merge(df_NatWald[["Geo_ID", "Bundesland"]], df_locations_metadata[["Geo_ID", "latitude", "longitude"]],  
                                    how='left', 
                                    on=["Geo_ID"]
                                ).reset_index(drop=True)

# %% Use center of federal states as locality if geopy cant find lat + lon
mask = df_NatWald_temp_merged["latitude"].isna()

geodata_federal_states_mapping = (pd.DataFrame.from_dict({
    "Baden-Württemberg" :	[48.661604, 9.350134],
    "Bayern" : [48.790447, 11.497889],
    "Berlin" : [52.520007, 13.404954],
    "Brandenburg" : [52.412529, 12.531644],
    "Bremen" : [53.079296, 8.801694],
    "Hamburg" : [53.551085, 9.993682],
    "Hessen" : [50.652051, 9.162438],
    "Mecklenburg-Vorpommern" : [53.612651, 12.429595],
    "Niedersachsen" : [52.636704, 9.845077],
    "Nordrhein-Westfalen" : [51.433237, 7.661594],
    "Rheinland-Pfalz" : [50.118346, 7.308953],
    "Saarland" : [49.396423, 7.022961],
    "Sachsen" : [51.104541, 13.201738],
    "Sachsen-Anhalt" : [51.950265, 11.692274],
    "Schleswig-Holstein" : [54.219367, 9.696117],
    "Thüringen": [47.198943, 9.764442]},
    columns = ["lat", "lon"],
    orient='index'
))

df_NatWald_temp_merged.loc[mask, "latitude"] = [geodata_federal_states_mapping.loc[x]["lat"] for x in df_NatWald_temp_merged.loc[mask, "Bundesland"]]
df_NatWald_temp_merged.loc[mask, "longitude"] = [geodata_federal_states_mapping.loc[x]["lon"] for x in df_NatWald_temp_merged.loc[mask, "Bundesland"]]

# %% Use Lon and Lat for geometry information

# df_NatWald = geopandas.GeoDataFrame(df_NatWald, 
#                                         geometry = geopandas.points_from_xy(df_NatWald_temp_merged["longitude"], 
#                                                                             df_NatWald_temp_merged["latitude"]))

df_NatWald["latitude"] = df_NatWald_temp_merged["latitude"].astype("category")
df_NatWald["longitude"] = df_NatWald_temp_merged["longitude"].astype("category")

# %% Use customer_sampleID as observation_id
df_NatWald["dataset"] = "NatWald"
df_NatWald["dataset"] = df_NatWald["dataset"].astype("category", copy=False)

df_NatWald["observation_id"] = df_NatWald['extra_target']

# %% Remove old target columns now that all data is extracted from them
df_NatWald.drop(axis=1, columns=["Kennziffer", "Geo_ID", "Leerung", "Bundesland", "extra_target"], inplace = True)

# %% Store the cleaned data
data_folder = "data/NatWald"
filename = "NatWald_a_posto"
Load_Store_Data().saving(df_NatWald, data_folder, filename, "parquet")

# %%
X = pd.DataFrame(Load_Store_Data().loading(data_folder, filename, "parquet"))
# %%
X.columns
# %%
X.head()

# %%
X.info()