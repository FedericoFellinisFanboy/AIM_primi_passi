def prime_counter_cy(range_from: int, range_til: int):
  """ Returns the number of found prime numbers using range"""
  prime_count = 0
  range_from = range_from if range_from >= 2 else 2
  for num in range(range_from, range_til + 1):
    for divnum in range(2, num):
      if ((num % divnum) == 0):
        break
    else:
      prime_count += 1
  return prime_count