# %% Convert the large xlsx file into a faster csv file - this takes some time..!
import pandas as pd
import numpy as np
import dask.dataframe as ddf
from dask.delayed import delayed

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %%
data_folder = "data/NatWald/"

year = "2022"
data_folder = f"data/LFU_AUM/{year}/"

filename = "LFU_AUM_custom_2022_results_JM3"

# %%
path = Load_Store_Data().get_path(data_folder, filename, "xlsx")

parts = delayed(pd.read_excel)(path)
df_per_dask = ddf.from_delayed(parts)

# %%
path = Load_Store_Data().get_path(data_folder, filename, "csv")

df_per_dask.to_csv(path, single_file = True, sep=";", index = False)
# %%
