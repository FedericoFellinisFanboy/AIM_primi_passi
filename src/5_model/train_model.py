# %% Load the packages
import pandas as pd
import numpy as np
import joblib
import os
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
#from xgboost import XGBClassifier
#import tensorflow as tf

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

from set_up_train_data import Set_Up_Train_Data

# %%
class Train_Model: 
    """Have all the model train steps combined in one class
    """
    def __init__(self, cluster_subtypes = None):
        self.load_store_data = Load_Store_Data()
        self.set_up_train_data = Set_Up_Train_Data(cluster_subtypes)
    
    def set_X_y_train(self):

        self.X_train, self.y_train = self.set_up_train_data.get_X_y_train()

        return self

    def get_X_y_train(self):

        return self.X_train, self.y_train

    def get_y_target(self):

        return self.set_up_train_data.get_y_target()

    def LogisticRegression_fit(self, _save_model : bool = True, **kwargs):

        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})
        
        self.lr = LogisticRegression(**kwargs).fit(self.X_train.values, self.y_train.values.ravel())

        if _save_model:
            data_folder = "models/LogisticRegression/"
            filename = f"first_model_{self.y_train.columns[0]}"
            self.load_store_data.saving(self.lr, data_folder, filename, "joblib")
            self.load_store_data.saving(self.__feature_names, data_folder, f"{filename}_feature_names", "parquet")

        return self
        
    def KNeighborsClassifier_fit(self, _save_model : bool = True, **kwargs):

        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})
        
        self.knn = KNeighborsClassifier(n_neighbors=3, **kwargs).fit(self.X_train.values, self.y_train.values.ravel())

        if _save_model:
            data_folder = "models/NearestNeighbors/"
            filename = f"first_model_{self.y_train.columns[0]}"
            self.load_store_data.saving(self.knn, data_folder, filename, "joblib")
            self.load_store_data.saving(self.__feature_names, data_folder, f"{filename}_feature_names", "parquet")

        return self

    def MLPClassifier_fit(self, _save_model : bool = True, **kwargs):

        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})

        self.mlp = MLPClassifier(max_iter=300, **kwargs).fit(self.X_train.values, self.y_train.values.ravel())

        if _save_model:
            data_folder = "models/NeuralNetwork/"
            filename = f"first_model_{self.y_train.columns[0]}"
            self.load_store_data.saving(self.mlp, data_folder, filename, "joblib")
            self.load_store_data.saving(self.__feature_names, data_folder, f"{filename}_feature_names", "parquet")

        return self

    def XGBClassifier_fit(self, _save_model : bool = True, **kwargs):

        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})
        
        self.xgb = XGBClassifier(verbose=2, **kwargs).fit(self.X_train.values, self.y_train.values.ravel())

        if _save_model:
            data_folder = "models/XGBoost/"
            filename = f"first_model_{self.y_train.columns[0]}"
            self.load_store_data.saving(self.xgb, data_folder, filename, "joblib")
            self.load_store_data.saving(self.__feature_names, data_folder, f"{filename}_feature_names", "parquet")

        return self

    def RandomForestClassifier_fit(self, _save_model : bool = True, **kwargs):
        
        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})
        
        self.rfc = RandomForestClassifier(**kwargs).fit(self.X_train.values, self.y_train.values.ravel())

        if _save_model:
            data_folder = "models/RandomForest/"
            filename = f"first_model_{self.y_train.columns[0]}"
            self.load_store_data.saving(self.rfc, data_folder, filename, "joblib")
            self.load_store_data.saving(self.__feature_names, data_folder, f"{filename}_feature_names", "parquet")

        return self

    def TensorFlow_Keras_fit(self, _save_model : bool = True, **kwargs):
        
        self.__feature_names = pd.DataFrame({"feature_names": list(self.X_train.columns)})

        # Define number of epochs and learning rate decay
        n_train = len(self.X_train)
        epochs = 10
        batch_size = 32
        steps_per_epoch = n_train // batch_size
        lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(
            0.01,
            decay_steps=steps_per_epoch*10,
            decay_rate=1,
            staircase=False)

        # Define optimizer used for modelling
        optimizer = tf.keras.optimizers.Adam(name='Adam')

        # Instantiate small model and print model summary
        with tf.device('/cpu:0'):
            self.tfk = tf.keras.Sequential([
                tf.keras.layers.Dense(256, kernel_initializer = 'uniform', activation='relu', input_dim = self.X_train.shape[1]),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Dense(16, kernel_initializer = 'uniform', activation='relu'),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Dense(len(set(self.y_train.values.ravel())), activation='softmax')])

            self.tfk.compile(optimizer=optimizer,
                            loss='categorical_crossentropy', metrics=["accuracy"])
        
            self.tfk.fit(self.X_train,
                        pd.get_dummies(self.y_train.astype(str)),
                        validation_split=0.2,
                        verbose=0,
                        epochs=epochs)

        # if _save_model:
        #     data_folder = "models/TensorFlow/"
        #     filename = f"first_model_{self.y_train.columns[0]}"
        #     large_model.save('saved_model/my_large_model')
        #     self.load_store_data.saving(self.rfc, data_folder, filename, "joblib")

        return self

    def RandomizedSearchCV_fit(self, _model, _param_distributions, _n_iter : int = 5, predefined_split : bool = False, **kwargs):
        
        if predefined_split:
            self.rscv = RandomizedSearchCV(_model, 
                                        param_distributions = _param_distributions, 
                                        random_state=0, 
                                        verbose=2, 
                                        n_iter = _n_iter,
                                        cv = self.set_up_train_data.ps,
                                        **kwargs).fit(self.X_train, self.y_train.values.ravel())
        else:
            self.rscv = RandomizedSearchCV(_model, 
                                        param_distributions = _param_distributions, 
                                        random_state=0, 
                                        verbose=2, 
                                        n_iter = _n_iter,
                                        **kwargs).fit(self.X_train, self.y_train.values.ravel())

        return self

    def get_X_y_test(self, X_test_raw, y_test_raw):

        X_test = X_test_raw
        X_test[[col for col in self.X_train.columns if not col in X_test_raw]] = 0

        return X_test[self.X_train.columns], y_test_raw[[self.set_up_train_data.get_y_target()]]

    def get_feature_names(self):

        return self.__feature_names


# %% 

# distributions_randomForest = {"n_estimators" : [200],
#                  "min_samples_split" : [4,5,7,8,9,10], 
#                  "min_samples_leaf" : [1,2,3,4,5,7,10,13,17,20] ,
#                  "max_depth": [15, 16, 17, 18, 19, 20,], 
#                  "criterion" : ['gini', "entropy"], 
#                 }

# distributions_xgb = {"eta" : [0.1,0.3,0.5],
#                  "gamma" : [0], 
#                  "min_child_weight" : [1,2,3,4,5,7,10,13,17,20] ,
#                  "max_depth": [10,12,13,14,15, 16, 17, 18, 19, 20,], 
#                  "subsample" : [0.5, 1], 
#                 }

# distributions_randomForest_short = {"n_estimators" : [200, 250],
#                  "min_samples_leaf" : [1,2,3,4,5],
#                  "criterion" : ['gini', "entropy"], 
#                 }

# %%
# https://www.backmarket.de/kopfhorer-mit-mikrophon-bang-olufsen-beoplay-h2-grauschwarz-gebraucht/477749.html#l=10
# %%
