# %% Load the packages
import pandas as pd
import numpy as np
import joblib
 
import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year, since_gts_intervall, consensus_score_threshold

sys.path.append('../5_model/')
from train_model import Train_Model

import helper

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
#from xgboost import XGBClassifier

# %% Load raw data
load_store_data = Load_Store_Data()

data_folder = "data/UnifiedData/"
X_raw = load_store_data.loading(data_folder, "bin_data_tutto", "parquet")
y_raw = load_store_data.loading(data_folder, "target_data_tutto", "parquet")
 
df_bin_consensus_family_connection= load_store_data.loading(data_folder, "bold_bin_consensus_family_connection", "parquet")

cluster_subtypes = ["forest", "agriculture"]
cluster_subtypes = ["forest"]
modeling = Train_Model(cluster_subtypes)

# %%
y_raw.loc[y_raw["dataset"] == "LandKLIF", "habitat_type"].value_counts()
# %%
y_target = ["habitat_type", "month", "semimonth", "since_gts", "latitude"]
y_target = "month"
data_scaling = "oneHot"

(modeling.set_up_train_data.reset_X_y_train()
                        .set_mask_via_target()
                        #.set_mask_via_target(dataset="LandKLIF")
                        # .set_mask_via_target(habitat_type = "forest_CF")
                        # .set_mask_via_target_OR(habitat_type = "forest_edge")
                        # .set_mask_via_target_OR(habitat_type = "forest_BL")
                        #.set_mask_via_target_OR(dataset = "LfU_AUM_2020")
                        #.set_mask_via_target_OR(dataset = "LfL_AUM")
                        #.set_mask_via_target_OR(dataset = "LandKLIF")
                        #.set_mask_via_target_AND(habitat_type = "forest")
                        .set_mask_on_train_data()
                        .set_y_target(y_target)
                        
                        #.set_X_train_consensus_families()
                        .train_test_split(0.2)
                        .store_X_y_train("data/TestData/train_test_split/")
                        .store_X_y_test("data/TestData/train_test_split/")
                        
                        .set_X_train_one_hot_encoding()
                        #.set_X_train_threshold_consensus_score(_consensus_score_threshold = {"A","yes"})
                        .set_bin_reads_one_hot_for_X_train(_min_bin_reads_one_hot = 50)#, _max_bin_reads_one_hot = 50)
                        
                        # #.set_X_train_normalize()
                        # #.set_X_train_fractionize()
                        # #.set_bin_reads_fraction_for_X_train(0.005)
                        # #.detect_outliers()

                        # .undersampling_X_y_train()
                        # .smote_X_y_train()
                        # .add_bias_node(1)
                        # .set_X_train_one_hot_encoding()
                        )
modeling.set_X_y_train()


# %%
A, y = modeling.get_X_y_train()
A.shape

# %%
# %%
print(f"Train for {modeling.y_train.columns[0]}")

randomForest_bestparams = dict(n_estimators = 2000,
                                            min_samples_split = 4,
                                            min_samples_leaf = 1,
                                            max_depth = 19,
                                            #max_samples = 0.8,
                                            #max_features = 0.3,
                                            criterion = 'gini')

# xgb_bestparams = dict(n_estimators = 200,
#                                             min_samples_split = 4,
#                                             min_samples_leaf = 1,
#                                             max_depth = 19,
#                                             criterion = 'gini')

rfc_cvparams = dict(max_samples = np.arange(0.5,1,0.1),
                                            #max_features = np.arange(0.1,0.5,0.1),
                                            max_features = [0.1],
                                            )
knn_cvparams = dict(n_neighbors = range(20))




mlp_bestparams = dict({'solver': 'adam',
   'learning_rate': 'adaptive',
   'hidden_layer_sizes': (100, 16  ), #(1024, 256, 16, ),
   'alpha': 1,
   'activation': 'relu',
   'early_stopping': True,})

mlp_cvparams = dict(hidden_layer_sizes = [(100,)],
                    activation = ['tanh', 'relu'],
                    solver = ['adam'],
                    alpha = [0.0001, 0.001, 0.01, 0.05, 0.1, 0.5, 1],
                    learning_rate = ['constant','adaptive'])


#model = modeling.RandomForestClassifier_fit(**randomForest_bestparams).rfc

#model = modeling.XGBClassifier_fit().xgb

#model = modeling.KNeighborsClassifier_fit().knn

model = modeling.MLPClassifier_fit(**mlp_bestparams).mlp

#model = modeling.TensorFlow_Keras_fit().tfk

#model = modeling.RandomizedSearchCV_fit(MLPClassifier(), mlp_cvparams, 10).rscv 
#model.cv_results_
# %%
model_columns = modeling.get_feature_names()["feature_names"]

# %% Train test split prediction
print(f"train_test_split - TRAIN - {y_target}")

#model_columns = model.get_booster().feature_names if "XGBClassifier" in str(type(model)) else model.feature_names_in_

data_folder = "data/TestData/train_test_split/"
X_test = load_store_data.loading(data_folder, "X_train", "parquet")
y_test = load_store_data.loading(data_folder, "y_train", "parquet")

X_test = helper.match_X_columns_with_model(X_test, model_columns, data_scaling)

y_test[[y_target]] = y_test[[y_target]].astype("category")

y_predict = pd.DataFrame(model.predict(X_test), columns=[y_target], dtype="category")

helper.get_heatplot(y_test, y_predict, "prediction/SimulateTestCases/", f"heatplot_train_split_{y_target}_clf")


# %%
# %% Train test split prediction
print(f"train_test_split - TEST - {y_target}")

#model_columns = model.get_booster().feature_names if "XGBClassifier" in str(type(model)) else model.feature_names_in_

data_folder = "data/TestData/train_test_split/"
X_test = load_store_data.loading(data_folder, "X_test", "parquet")
y_test = load_store_data.loading(data_folder, "y_test", "parquet")

X_test = helper.match_X_columns_with_model(X_test, model_columns, data_scaling)

y_test[[y_target]] = y_test[[y_target]].astype("category")

y_predict = pd.DataFrame(model.predict(X_test.values), columns=[y_target], dtype="category")

helper.get_heatplot(y_test, y_predict, "prediction/SimulateTestCases/", f"heatplot_test_split_{y_target}_clf")


# %% Simulate test cases with datasets
print(f"simulate test cases with datasets - {y_target}")

data_folder = "data/UnifiedData/"

X_test = load_store_data.loading(data_folder, "bin_data_tutto", "parquet")
y_raw = load_store_data.loading(data_folder, "target_data_tutto", "parquet")

X_test = helper.match_X_columns_with_model(X_test, model_columns, data_scaling)

datasets = ["LandKLIF", "LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021", "LfU_AUM_2022", "NatWald"]
datasets = ["LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021"]

for dataset in datasets:
    y_test = y_raw[[y_target]] // since_gts_intervall if y_target == "since_gts" else y_raw[[y_target]]

    print(dataset)
    mask = (y_raw["dataset"] == dataset) & (y_raw[y_target].notna())
    
    y_test = y_test[mask].round(0).astype("int") if y_target == "latitude" else y_test[mask].astype("category")
    y_predict = pd.DataFrame(model.predict(X_test[mask].values), columns=[y_target], dtype="category", index = X_test[mask].index)
    
    helper.get_heatplot(y_test, y_predict, "prediction/SimulateTestCases/", f"heatplot_{dataset}_{y_target}_rfc", y_raw.loc[mask])


# %%

# %% Dimensionality reduction
from sklearn.decomposition import TruncatedSVD
import plotly.express as plx
from plotly.subplots import make_subplots
import plotly.graph_objects as go

dimensionali_reduction = True

if dimensionali_reduction:

    svd = TruncatedSVD(random_state=42)
    X_svd = pd.DataFrame(svd.fit_transform(X_test[mask]), index=X_test[mask].index)

    title_string = f"Separatation of the Data into True and False Predictions ({dataset})"

    fig = make_subplots(rows=2, cols=2)
    for group in sorted(set(y_test[y_target])):
        mask_group = y_test[y_target] == group
        subplot = go.Scatter(x = X_svd.loc[mask_group, 0], y = X_svd.loc[mask_group, 1],
                            #name=f"{group.month_name()}",
                            name=f"{group}",
                            mode="markers",
                            customdata = np.stack((y_raw.loc[mask, "observation_id"], [f"{group}"] * len(X_test[mask])), axis=-1),
                            hovertemplate=
                                "id: %{customdata[0]}<br>" +
                                "prediction: %{customdata[1]}<br>" +
                                "<extra></extra>",
                            )
        fig.add_trace(subplot, row = 1, col = 1)

    for group in sorted(set(y_predict[y_target])):
        mask_group = y_predict[y_target] == group
        subplot = go.Scatter(x = X_svd.loc[mask_group, 0], y = X_svd.loc[mask_group, 1],
                            #name=f"{group.month_name()}_p",
                            name=f"{group}_p",
                            mode="markers",
                            customdata = np.stack((y_raw.loc[mask, "observation_id"], [f"{group}_p"] * len(X_test[mask])), axis=-1),
                            hovertemplate=
                                "id: %{customdata[0]}<br>" +
                                "prediction: %{customdata[1]}<br>" +
                                "<extra></extra>",
                            )
        fig.add_trace(subplot, row = 1, col = 2)

    for group in sorted(set(y_test[y_target])):
        mask_group = (y_test[y_target] == group) & (y_test[y_target].astype(str) == y_predict[y_target].astype(str))
        subplot = go.Scatter(x = X_svd.loc[mask_group, 0], y = X_svd.loc[mask_group, 1],
                            #name=f"{group.month_name()}",
                            name=f"{group}",
                            mode="markers",
                            customdata = np.stack((y_raw.loc[mask, "observation_id"], [f"{group}"] * len(X_test[mask])), axis=-1),
                            hovertemplate=
                                "id: %{customdata[0]}<br>" +
                                "prediction: %{customdata[1]}<br>" +
                                "<extra></extra>",
                            )
        fig.add_trace(subplot, row = 2, col = 1)

    for group in sorted(set(y_predict[y_target])):
        mask_group = (y_predict[y_target] == group) & (y_test[y_target].astype(str) != y_predict[y_target].astype(str))
        subplot = go.Scatter(x = X_svd.loc[mask_group, 0], y = X_svd.loc[mask_group, 1],
                            #name=f"{group.month_name()}_p",
                            name=f"{group}_p",
                            mode="markers",
                            customdata = np.stack((y_raw.loc[mask, "observation_id"], [f"{group}_p"] * len(X_test[mask])), axis=-1),
                            hovertemplate=
                                "id: %{customdata[0]}<br>" +
                                "prediction: %{customdata[1]}<br>" +
                                "<extra></extra>",
                            )
        fig.add_trace(subplot, row = 2, col = 2)

    fig.update_layout(title_text=title_string)

    data_folder = "/prediction/DimensionReduction"
    Load_Store_Data().saving(fig, data_folder, title_string, "html")

# %%

# %% Real test cases
#rint(f"real test cases - {y_target}")

data_folder = "data/TestData/TestCases/"
test_cases = ["Braunisch", "Eutropia", "Heibl", "Kaczmarek", "Lobinger"]
test_cases = ["Heibl"]

for test_case in test_cases:
    print(test_case)

    filename = f"{test_case}_a_posto"
    X_test = load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet")
    y_raw = load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet")
    
    X_test = helper.match_X_columns_with_model(X_test, model_columns, data_scaling)
 
    y_test = y_raw[[y_target]] // since_gts_intervall if y_target == "since_gts" else y_raw[[y_target]]

    mask = y_test[y_target].notna()

    y_test = y_test[mask].astype("category")
    y_predict = pd.DataFrame(model.predict(X_test[mask].values), columns=[y_target], dtype="category")

    helper.get_heatplot(y_test, y_predict, "prediction/TestCases/", f"heatplot_{test_case}_{y_target}_rfc", y_raw)
   
# %%

# %%
# mask = y_raw["dataset"] == dataset_to_check

# data_folder = "data/UnifiedData/"
# filename = "bin_data_tutto"
# X_test_raw = load_store_data.read_raw_parquet(data_folder, filename, columns = xgb_model.get_booster().feature_names)
# X_test = X_test_raw[mask]
# X_dummy_test = X_test.copy()
# X_dummy_test[X_test>0] = 1

# # %% Scale the test data
# X_test = pd.DataFrame(X_test_raw.divide(y_raw.loc[y_test.index, "sum_raw_reads"], axis="index"), columns = X_test_raw.columns, index = X_test_raw.index)

# %%
