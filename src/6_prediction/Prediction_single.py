# %%
print("Get the BINs for prediction from '/prediction/BOLD_BIN_uri_for_prediction.csv' and their counts (if available) from '/prediction/BOLD_BIN_uri_counts_for_prediction.csv'")
print("Write the predictions as likelihoods into '/prediction/Predictions_single_Results.csv'")

# %%
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')

import load_store_data

# %% Load the model
import joblib

data_folder_model = "models/RandomForest/"
filename = f"first_model_{y_test.columns[0]}"
filepath = os.path.expanduser(load_store_data.get_path(data_folder_model, filename, "joblib"))

clf_model = joblib.load(filepath)

filename = f"first_dummy_model_{y_test.columns[0]}"
filepath = os.path.expanduser(load_store_data.get_path(data_folder_model, filename, "joblib"))

clf_dummy_model = joblib.load(filepath)

# %% Load the test data

data_folder = "prediction/"
filename = "BOLD_BIN_uri_for_prediction"
df_test_raw = load_store_data.read_raw_csv(data_folder, filename, header=None)

try:
    filename = "BOLD_BIN_uri_counts_for_prediction"
    df_counts = load_store_data.read_raw_csv(data_folder, filename, header=None)
    df_test_raw[1] = df_counts
except:
    pass

df_test = df_test_raw.T
df_test.columns = df_test.iloc[0].str.replace("BOLD:", "")
df_test = df_test[1:]

# %% Scale the test data
X_test = pd.DataFrame(X_test_raw.divide(y_raw.loc[y_test.index, "sum_raw_reads"], axis="index"), columns = X_test_raw.columns, index = X_test_raw.index)

# %%
if df_test.shape[0] > 0:

    X_test = pd.DataFrame(0, index=df_test.index, columns = X_train.keys())

    for col in df_test.keys():
        if col in X_train.keys():
            X_test[col] = df_test[col]

    X_test = scaler.transform(X_test)

    y_predict = pd.DataFrame(clf.predict_proba(X_test), columns = clf_dummy.classes_, index = None)
    y_predict.to_csv("Predictions_single_Results.csv")

else:

    X_test = pd.DataFrame(0, index=[0], columns = X_train.keys())

    for col in df_test.keys():
        X_test[col] = 1

    y_predict = pd.DataFrame(clf_dummy.predict_proba(X_test), columns = clf_dummy.classes_, index = None)
    y_predict.to_csv("Predictions_single_Results.csv")
