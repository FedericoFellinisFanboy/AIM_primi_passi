asttokens==2.0.5
backcall==0.2.0
black==21.12b0
certifi==2021.10.8
click==8.0.3
click-plugins==1.1.1
Cython==0.29.26
cytoolz==0.11.0
dask==2022.1.0
debugpy==1.5.1
decorator==5.1.1
distributed==2022.1.0
entrypoints==0.3
et-xmlfile==1.1.0
executing==0.8.2
GDAL==3.4.1
geographiclib==1.52
geopy==2.2.0
imbalanced-learn==0.9.0
imblearn==0.0
ipykernel==6.7.0
ipython==8.0.0
jedi==0.18.1
jupyter-client==7.1.1
kaleido==0.2.1
locket==0.2.1
matplotlib-inline==0.1.3
mkl-fft==1.3.1
mkl-service==2.4.0
munch==2.5.0
munkres==1.1.4
mypy-extensions==0.4.3
nest-asyncio==1.5.4
numpy==1.22.1
openpyxl==3.0.9
pandas==1.3.5
parso==0.8.3
pathspec==0.9.0
pexpect==4.8.0
pickleshare==0.7.5
Pillow==8.4.0
platformdirs==2.4.1
plotly==5.5.0
prompt-toolkit==3.0.24
ptyprocess==0.7.0
pure-eval==0.2.1
pyarrow==6.0.1
Pygments==2.11.2
pyparsing==3.0.6
pytz==2021.3
PyYAML==6.0
pyzmq==22.3.0
seaborn==0.11.2
stack-data==0.1.4
tomli==1.2.3
typing_extensions==4.0.1
wcwidth==0.2.5
wordcloud==1.8.1
xgboost==1.5.2
zict==2.0.0
